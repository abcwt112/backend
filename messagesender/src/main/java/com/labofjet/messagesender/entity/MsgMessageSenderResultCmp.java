package com.labofjet.messagesender.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 消息发送结果
 */
@Document(collection = "msg_message_sender_result")
public class MsgMessageSenderResultCmp {
	@Id
	private String id;

	private String result; // 发送结果

	private Date date; // 发送日期

	private String username;

	private String sn; // 编号

	private String failMessage;

	public String getFailMessage() {
		return failMessage;
	}

	public void setFailMessage(String failMessage) {
		this.failMessage = failMessage;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "MsgMessageSenderResultCmp{" +
				"id='" + id + '\'' +
				", result='" + result + '\'' +
				", date=" + date +
				", username='" + username + '\'' +
				", sn='" + sn + '\'' +
				'}';
	}
}