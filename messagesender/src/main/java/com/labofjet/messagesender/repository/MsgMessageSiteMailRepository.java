package com.labofjet.messagesender.repository;

import com.labofjet.messagesender.entity.MsgMessageSiteMailCmp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsgMessageSiteMailRepository extends MongoRepository<MsgMessageSiteMailCmp, String> {


}
