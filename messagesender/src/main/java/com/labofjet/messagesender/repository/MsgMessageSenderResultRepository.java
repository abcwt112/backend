package com.labofjet.messagesender.repository;

import com.labofjet.messagesender.entity.MsgMessageSenderResultCmp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsgMessageSenderResultRepository extends MongoRepository<MsgMessageSenderResultCmp, String> {


}
