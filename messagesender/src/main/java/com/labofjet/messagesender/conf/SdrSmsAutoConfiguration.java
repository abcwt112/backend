package com.labofjet.messagesender.conf;

import com.labofjet.messagesender.utils.SmsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SdrSmsProperties.class)
public class SdrSmsAutoConfiguration {

    @Autowired
    private SdrSmsProperties sdrSmsProperties;

    @Bean
    public SmsUtils smsUtils() {
        SmsUtils utils = new SmsUtils();
        utils.setAccessKeyId(sdrSmsProperties.getAccessKeyId());
        utils.setAccessKeySecret(sdrSmsProperties.getAccessKeySecret());
        utils.setDomain(sdrSmsProperties.getDomain());
        utils.setProduct(sdrSmsProperties.getProduct());
        utils.setRegionId(sdrSmsProperties.getRegionId());
        utils.setEndpointName(sdrSmsProperties.getEndpointName());

        utils.initClient();

        System.setProperty("sun.net.client.defaultConnectTimeout", sdrSmsProperties.getDefaultConnectTimeout());
        System.setProperty("sun.net.client.defaultReadTimeout", sdrSmsProperties.getDefaultReadTimeout());

        return utils;
    }
}

