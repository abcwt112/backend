package com.labofjet.messagesender.conf;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * 启动类
 */
@SpringBootApplication
@Import(com.labofjet.message.conf.MessageMain.class)
@EnableRabbit
@EnableMongoRepositories(basePackages = "com.labofjet")
@EnableDiscoveryClient
public class MessagerSenderMain {

	public static void main(String[] args) {
		SpringApplication.run(MessagerSenderMain.class, args);
	}

}