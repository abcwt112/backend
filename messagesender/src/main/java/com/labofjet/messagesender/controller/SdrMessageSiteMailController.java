package com.labofjet.messagesender.controller;

import com.labofjet.common.controller.BaseController;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.dto.PageDto;
import com.labofjet.message.dto.MsgMessageSiteMailDto;
import com.labofjet.messagesender.service.impl.SdrMessageSiteMailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * common页面信息
 */
@RestController
public class SdrMessageSiteMailController extends BaseController {


    @Autowired
    private SdrMessageSiteMailServiceImpl sdrMessageSiteMailService;

    @RequestMapping("/messagesitemail/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
        contextDto.setSuccess(true);
        PageDto<MsgMessageSiteMailDto> list = sdrMessageSiteMailService.list(contextDto);
        contextDto.setData(list);
        return contextDto;
    }

    @RequestMapping("/messagesitemail/load")
    public ContextDto load(@RequestBody ContextDto contextDto) {
        contextDto.setSuccess(true);
        MsgMessageSiteMailDto dto = sdrMessageSiteMailService.load(contextDto);
        contextDto.setData(dto);
        return contextDto;
    }

    @RequestMapping("/messagesitemail/count")
    public ContextDto count(@RequestBody ContextDto contextDto) {
        contextDto.setSuccess(true);
        Long count = sdrMessageSiteMailService.count(contextDto);
        contextDto.setData(count);
        return contextDto;
    }

    @RequestMapping("/messagesitemail/readUnRead")
    public ContextDto save(@RequestBody ContextDto contextDto) {
        contextDto.setSuccess(true);
        MsgMessageSiteMailDto msgMessageSiteMailDto = sdrMessageSiteMailService.readUnRead(contextDto);
        contextDto.setData(msgMessageSiteMailDto);
        return contextDto;
    }

}
