package com.labofjet.messagesender.controller;

import com.labofjet.common.controller.BaseController;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.dto.PageDto;
import com.labofjet.message.dto.MsgMessageSenderResultDto;
import com.labofjet.messagesender.service.impl.SdrMsgMessageSenderInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SdrMsgMessageSenderInfoController extends BaseController {

    @Autowired
    private SdrMsgMessageSenderInfoServiceImpl sdrMsgMessageSenderInfoService;

    @RequestMapping("/messagesenderinfo/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
        contextDto.setSuccess(true);
        PageDto<MsgMessageSenderResultDto> page = sdrMsgMessageSenderInfoService.list(contextDto);
        contextDto.setData(page);
        return contextDto;
    }

}
