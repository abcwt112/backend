package com.labofjet.messagesender.handler;


import com.labofjet.common.handler.impl.BaseMqHandlerImpl;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.message.enums.EMessageTypeDicType;
import com.labofjet.messagesender.service.SdrMessageSenderService;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 发送消息,并记录
 */
@Service
@RabbitListener(bindings = {
		@QueueBinding(value = @Queue(value = "messageQueue", durable = "true"), exchange = @Exchange(value = "messageExchange", durable = "true"))
})
public class SdrMessageSenderHandler extends BaseMqHandlerImpl {

	@Autowired
	@Qualifier("sdrEMailMessageSenderServiceImpl")
	private SdrMessageSenderService sdrMailMessageSenderServiceImpl;

	@Autowired
	@Qualifier("sdrSiteMailMessageSenderServiceImpl")
	private SdrMessageSenderService sdrSiteMailMessageSenderServiceImpl;

	@Autowired
	@Qualifier("sdrShortMessageSenderServiceImpl")
	private SdrMessageSenderService sdrShortMessageSenderServiceImpl;

	@Override
	public void handle(String s) {
		MsgMessageSenderDto msgDto = JSONUtils.stringToDto(s, MsgMessageSenderDto.class);
		String type = msgDto.getType();
		if (EMessageTypeDicType.SITE_MAIL.getCode().equals(type)) { // 站内信
			sdrSiteMailMessageSenderServiceImpl.handle(msgDto);
		} else if (EMessageTypeDicType.EMAIL.getCode().equals(type)) { // email
			sdrMailMessageSenderServiceImpl.handle(msgDto);
		} else if (EMessageTypeDicType.MESSAGE.getCode().equals(type)) { // 短信
			sdrShortMessageSenderServiceImpl.handle(msgDto);
		}
	}

}
