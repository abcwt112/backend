package com.labofjet.messagesender.service.impl;

import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.system.dto.SysUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * 邮件服务
 */
@Service
public class SdrEMailMessageSenderServiceImpl extends SdrBaseMessageSenderServiceImpl {

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private MailProperties mailProperties;

	@Override
	protected MsgMessageSenderDto handlerInternal(MsgMessageSenderDto msgMessageSenderDto, SysUserDto sysUserDtos) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(mailProperties.getUsername());
		message.setTo(sysUserDtos.getEmail());
		message.setSubject(msgMessageSenderDto.getTopic());
		message.setText(this.processText(msgMessageSenderDto.getMessage(), msgMessageSenderDto, sysUserDtos));
		message.setCc(mailProperties.getUsername()); // 抄送自己
		mailSender.send(message);

		// TODO 成功失败都应该保存数据
		return msgMessageSenderDto;
	}


}

