package com.labofjet.messagesender.service;

import org.springframework.cloud.netflix.feign.FeignClient;


@FeignClient(value = "aliShortMessageSender", url = "http://localhost/messagesender")
public interface AliShortMessageSender {
}
