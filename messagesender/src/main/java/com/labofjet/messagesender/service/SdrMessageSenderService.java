package com.labofjet.messagesender.service;


import com.labofjet.message.dto.MsgMessageSenderDto;

public interface SdrMessageSenderService {
	MsgMessageSenderDto handle(MsgMessageSenderDto msgMessageSenderDto);
}
