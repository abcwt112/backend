package com.labofjet.messagesender.service.impl;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.dto.PageDto;
import com.labofjet.common.service.impl.BaseServiceImpl;
import com.labofjet.common.util.MongoQueryUtils;
import com.labofjet.message.dto.MsgMessageSenderResultDto;
import com.labofjet.messagesender.entity.MsgMessageSenderResultCmp;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SdrMsgMessageSenderInfoServiceImpl extends BaseServiceImpl {

    @Resource
    private MongoTemplate mongoTemplate;

    public PageDto<MsgMessageSenderResultDto> list(ContextDto contextDto){
        PageDto<MsgMessageSenderResultDto> page =  MongoQueryUtils.getInstance(contextDto,mongoTemplate).
                and("sn").paging().sort().find(MsgMessageSenderResultCmp.class, MsgMessageSenderResultDto.class);
        return page;
    }
}
