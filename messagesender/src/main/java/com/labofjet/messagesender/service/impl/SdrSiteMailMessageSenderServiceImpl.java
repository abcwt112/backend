package com.labofjet.messagesender.service.impl;

import com.labofjet.common.util.ConverterUtils;
import com.labofjet.common.util.IdUtils;
import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.messagesender.entity.MsgMessageSiteMailCmp;
import com.labofjet.messagesender.repository.MsgMessageSiteMailRepository;
import com.labofjet.system.dto.SysUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 站内信
 */
@Service
public class SdrSiteMailMessageSenderServiceImpl extends SdrBaseMessageSenderServiceImpl {

    @Autowired
    private MsgMessageSiteMailRepository msgMessageSiteMailRepository;

    @Override
    protected MsgMessageSenderDto handlerInternal(MsgMessageSenderDto msgMessageSenderDto, SysUserDto sysUserDto) {
        // 保存信息DTO到mongodb
        MsgMessageSiteMailCmp siteMailCmp = ConverterUtils.convertOne(msgMessageSenderDto, new MsgMessageSiteMailCmp());
        siteMailCmp.setId(IdUtils.uuid());
        siteMailCmp.setRead(false);
        siteMailCmp.setReceiverUsername(sysUserDto.getUsername());
        siteMailCmp.setReceiverId(sysUserDto.getId());
        siteMailCmp.setMessage(this.processText(siteMailCmp.getMessage(), msgMessageSenderDto, sysUserDto));
        msgMessageSiteMailRepository.save(siteMailCmp);
        return msgMessageSenderDto;
    }


}

