<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${packageName}.mapper.${class}Mapper">
	<sql id="baseColumn">
		${baseColumns}
	</sql>

	<sql id="condition">
		<#list fieldsInfo as f>
			<if test="${f.javaName} != null and ${f.javaName} != ''">
				and t.${f.field} = <#noparse>#{</#noparse> ${f.javaName} <#noparse>}</#noparse>
			</if>
		</#list>
	</sql>

	<sql id="baseSelect">
		select <include refid="baseColumn"></include> from ${table} t
		where 1=1
		<include refid="condition"></include>
	</sql>

	<select id="selectList" resultType="${class}Dto" parameterType="hashmap">
		select t.*  from (<include refid="baseSelect"></include>) t

		<!-- left join sys_menu a on a.name = t.menuSn -->
		<!-- <if test="menuName != null and menuName != ''">
			and a.value = <#noparse>#{menuName}</#noparse>
		</if> -->
	</select>
</mapper>