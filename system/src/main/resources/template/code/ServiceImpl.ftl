package ${packageName}.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.system.service.impl.CrudBaseServiceImpl
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import ${packageName}.dto.${class}Dto;
import ${packageName}.dto.${class}Example;
import ${packageName}.mapper.${class}Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
* ${tableInfo.comment}  serviceImpl类
*/
@Service
public class ${class}ServiceImpl extends CrudBaseServiceImpl<${class}Dto> {
	@Resource
	private ${class}Mapper ${variable}Mapper;

	public List<${class}Dto> listPage(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<${class}Dto> ${variable}Dtos = ${variable}Mapper.selectList(map);
		return ${variable}Dtos;
	}

	@Override
	public PageInfo<${class}Dto> list(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<${class}Dto> ${class}Dtos = ${variable}Mapper.selectList(map);
		return new PageInfo<>(${class}Dtos);
	}

	@Override
	public ${class}Dto load(ContextDto result) {
		Map<String, Object> map = super.getRequestParamsMap(result);
		Integer id = (Integer) map.get("id");
		if (null != id) {
			return ${variable}Mapper.selectList(map).get(0);
		}
		return null;
	}

	@Override
	@Transactional
	public ${class}Dto save(ContextDto result) {
		${class}Dto dto = JSONUtils.getRequestData(result, ${class}Dto.class);
		if (dto.getId() != null) {
			${variable}Mapper.updateByPrimaryKey(dto);
		} else {
			${variable}Mapper.insertSelective(dto);
		}
		return dto;
	}

	@Override
	@Transactional
	public int delete(ContextDto result) {
	List<Integer> ids = JSONUtils.getRequestData(result, List.class);
		${class}Example example = new ${class}Example();
		example.createCriteria().andIdIn(ids);
		int i = ${variable}Mapper.deleteByExample(example);
		return i;
	}
}
