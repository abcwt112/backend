<style lang="less">
	/*@import './international.less';*/
</style>

<template>
	<div>
		<common-page v-bind="menuPage">
			<template slot="modal">
				<Form :model="menuPage.detailData" :label-width="80">
                    <#list fieldsInfo as f>
						<FormItem label="${f.comment}">
							<Input v-model="menuPage.detailData.${f.javaName}"></Input>
						</FormItem>
                    </#list>
				</Form>
			</template>
		</common-page>
	</div>
</template>

<script>

	import commonPage from '@/views/common/common-page.vue';
	import util from '@/libs/util';
	import autoSelect from '@/views/common/autoSelect.vue';

	// 默认界面查询条件
	let pageParams = Object.assign({}, util.queryParams, { // 额外的参数,比如条件的默认值
		sortName: "id",
		sortOrder: "asc"
	});
	export default {
		name: '${url}-page',
		components: {
			commonPage
		},
		props: {
			columns: Array
		},
		data () {
			return {
				menuPage: {
					toolbar: {
						menuSn: "${url}",
						buttons: []
					},
					list: {
						table: {
							columns: [
							    {type: 'selection', width: 50, align: 'center'},
								{type: 'index', width: 60, align: 'center'},
								<#list fieldsInfo as f>
									{title: '${f.comment}', key: '${f.javaName}'}<#if f_has_next>,</#if>
								</#list>
                            ],
                            data: []
                        },
                        url: "${url}/list",
                        queryParams: pageParams
                    },
					searchbar: {
						queryParams: pageParams,
						inputs: [
						<#list fieldsInfo as f>
							{
								text: "${f.comment}",
								sn: "${f.javaName}",
								style: {
									width: "200px"
								}
							}<#if f_has_next>,</#if>
						</#list>
						]
					},
					pageParams: pageParams,
						detailData: {
							<#list fieldsInfo as f>
								${f.javaName}:""<#if f_has_next>,</#if>
							</#list>
						}
					}
				};
		},
		methods: {},
		created () {
		},
		mounted() {
		}
	};
</script>
