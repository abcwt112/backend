package ${packageName}.mapper;

import com.labofjet.common.dto.ContextDto;
import ${packageName}.dto.${class}Dto;
import ${packageName}.dto.${class}Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ${class}Mapper {
    int countByExample(${class}Example example);

    int deleteByExample(${class}Example example);

    int deleteByPrimaryKey(Integer id);

    int insert(${class}Dto record);

    int insertSelective(${class}Dto record);

    List<${class}Dto> selectByExample(${class}Example example);

    ${class}Dto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") ${class}Dto record, @Param("example") ${class}Example example);

	int updateByExample(@Param("record") ${class}Dto record, @Param("example") ${class}Example example);

	int updateByPrimaryKeySelective(${class}Dto record);

	int updateByPrimaryKey(${class}Dto record);

	//custom
	List<${class}Dto> selectList(Map map);
}