package ${packageName}.controller;

import com.labofjet.common.controller.BaseController;
import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import ${packageName}.dto.${class}Dto;
import ${packageName}.service.impl.${class}ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
*  ${tableInfo.comment}  controller类
*/
@RestController
public class ${class}Controller extends BaseController {

    @Autowired
    private ${class}ServiceImpl ${variable}Service;

    @RequestMapping("/${url}/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
    PageInfo<${class}Dto> ${variable}Dtos = ${variable}Service.list(contextDto);
        contextDto.setData(${variable}Dtos);
        contextDto.setSuccess(true);
        return contextDto;
	}

	@RequestMapping("/${url}/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
        ${class}Dto ${variable}Dtos = ${variable}Service.load(contextDto);
        contextDto.setData(${variable}Dtos);
        contextDto.setSuccess(true);
        return contextDto;
	}

	@RequestMapping("/${url}/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
        ${class}Dto ${variable}Dtos = ${variable}Service.save(contextDto);
        contextDto.setData(${variable}Dtos);
        contextDto.setSuccess(true);
	    return contextDto;
	}

	@RequestMapping("/${url}/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
        int i = ${variable}Service.delete(contextDto);
        contextDto.setSuccess(true);
        contextDto.setMessage("成功删除了" + i + "条数据");
        return contextDto;
	}
}
