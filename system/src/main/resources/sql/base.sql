/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : base

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-11-30 18:51:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id`    INT(11) NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(255)     DEFAULT NULL,
  `pid`   INT(11)          DEFAULT NULL,
  `state` VARCHAR(4)       DEFAULT '0'
  COMMENT '1=有效 2=无效',
  `icon`  VARCHAR(50)      DEFAULT NULL,
  `name`  VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', null, '1', 'settings', 'system');
INSERT INTO `sys_menu` VALUES ('2', '菜单管理', '1', '1', 'android-menu', 'menu');
INSERT INTO `sys_menu` VALUES ('3', '用户管理', '1', '1', 'ios-people', 'user');
INSERT INTO `sys_menu` VALUES ('20', '页面管理', '1', '1', 'ios-book-outline', 'page');
INSERT INTO `sys_menu` VALUES ('22', '角色管理', '1', '1', 'ios-body-outline', 'role');
INSERT INTO `sys_menu` VALUES ('23', '权限管理', '1', '1', 'checkmark-circled', 'permission');

-- ----------------------------
-- Table structure for sys_page
-- ----------------------------
DROP TABLE IF EXISTS `sys_page`;
CREATE TABLE `sys_page` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT,
  `menu_sn` VARCHAR(255)     DEFAULT NULL
  COMMENT 'menu的sn(name)',
  `sn`      VARCHAR(255)     DEFAULT NULL
  COMMENT '每个页面唯一',
  `text`    VARCHAR(255)     DEFAULT NULL
  COMMENT '名称',
  `type`    VARCHAR(255)     DEFAULT NULL
  COMMENT 'button',
  `state`   VARCHAR(4)       DEFAULT NULL,
  `sort`    INT(4)           DEFAULT NULL
  COMMENT '顺序',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_page
-- ----------------------------
INSERT INTO `sys_page` VALUES ('1', 'menu', 'add', '新增', 'toolbar', '1', '1');
INSERT INTO `sys_page` VALUES ('2', 'menu', 'edit', '编辑', 'rowbar', '1', '3');
INSERT INTO `sys_page` VALUES ('3', 'menu', 'detail', '详情', 'rowbar', '1', '4');
INSERT INTO `sys_page` VALUES ('4', 'menu', 'delete', '删除', 'toolbar', '1', '2');
INSERT INTO `sys_page` VALUES ('5', 'page', 'add', '新增', 'toolbar', '1', '1');
INSERT INTO `sys_page` VALUES ('6', 'page', 'edit', '编辑', 'rowbar', '1', '3');
INSERT INTO `sys_page` VALUES ('7', 'page', 'detail', '详情', 'rowbar', '1', '4');
INSERT INTO `sys_page` VALUES ('8', 'page', 'delete', '删除', 'toolbar', '1', '2');
INSERT INTO `sys_page` VALUES ('12', 'role', 'add', '新增', 'toolbar', '1', '1');
INSERT INTO `sys_page` VALUES ('13', 'role', 'detail', '详情', 'rowbar', '1', '2');
INSERT INTO `sys_page` VALUES ('14', 'role', 'edit', '编辑', 'rowbar', '1', '3');
INSERT INTO `sys_page` VALUES ('15', 'role', 'delete', '删除', 'toolbar', '1', '4');
INSERT INTO `sys_page` VALUES ('16', 'user', 'add', '新增', 'toolbar', '1', '1');
INSERT INTO `sys_page` VALUES ('17', 'user', 'delete', '删除', 'toolbar', '1', '2');
INSERT INTO `sys_page` VALUES ('18', 'user', 'edit', '编辑', 'rowbar', '1', '3');
INSERT INTO `sys_page` VALUES ('19', 'user', 'detail', '详情', 'rowbar', '1', '4');
INSERT INTO `sys_page` VALUES ('20', 'permission', 'add', '新增', 'toolbar', '1', '1');
INSERT INTO `sys_page` VALUES ('21', 'permission', 'delete', '删除', 'toolbar', '1', '2');
INSERT INTO `sys_page` VALUES ('22', 'permission', 'edit', '编辑', 'rowbar', '1', '3');
INSERT INTO `sys_page` VALUES ('23', 'permission', 'detail', '详情', 'rowbar', '1', '4');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id`    INT(11) NOT NULL AUTO_INCREMENT,
  `sn`    VARCHAR(255)     DEFAULT NULL,
  `text`  VARCHAR(255)     DEFAULT NULL
  COMMENT 'url,menu,page',
  `type`  VARCHAR(255)     DEFAULT NULL,
  `state` VARCHAR(4)       DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 11
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', '/menu', '菜单', 'url', '1');
INSERT INTO `sys_permission` VALUES ('2', '/menu/list', '菜单列表', 'url', '1');
INSERT INTO `sys_permission` VALUES ('3', 'system', '系统管理菜单', 'menu', '1');
INSERT INTO `sys_permission` VALUES ('4', 'user', '用户菜单', 'menu', '1');
INSERT INTO `sys_permission` VALUES ('5', 'page', '页面菜单', 'menu', '1');
INSERT INTO `sys_permission` VALUES ('6', 'role', '角色菜单', 'menu', '1');
INSERT INTO `sys_permission` VALUES ('7', 'permission', '权限菜单', 'menu', '1');
INSERT INTO `sys_permission` VALUES ('8', '/menu/load', '菜单数据', 'url', '1');
INSERT INTO `sys_permission` VALUES ('9', '/menu/save', '菜单保存', 'url', '1');
INSERT INTO `sys_permission` VALUES ('10', '/menu/delete', '菜单删除', 'url', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `sn`   VARCHAR(255)     DEFAULT NULL,
  `text` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '管理员');
INSERT INTO `sys_role` VALUES ('2', 'guest', '游客');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `role_id`       INT(11)          DEFAULT NULL,
  `permission_id` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('2', '1', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255)     DEFAULT NULL,
  `password` VARCHAR(255)     DEFAULT NULL,
  `email`    VARCHAR(255)     DEFAULT NULL,
  `state`    VARCHAR(4)       DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'jyzjyz12@163.com', '1');
INSERT INTO `sys_user` VALUES ('2', 'guest', 'e10adc3949ba59abbe56e057f20f883e', 'jyzjyz12@163.com', '1');
INSERT INTO `sys_user` VALUES ('3', 'test', 'f2750fc6d623392c1c8ad1d9d18f7ea5', '', '2');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11)          DEFAULT NULL,
  `role_id` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
