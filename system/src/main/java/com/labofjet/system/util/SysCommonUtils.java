package com.labofjet.system.util;

import com.labofjet.common.util.CommonUtils;
import com.labofjet.system.dto.SysUserDto;
import com.labofjet.system.enums.ESessionDefine;

import javax.servlet.http.HttpServletRequest;

/**
 * 常用方法
 */
public class SysCommonUtils {

	public static SysUserDto getCurrentUser() {
		HttpServletRequest request = CommonUtils.getCurrentRequest();
		if (request == null || request.getSession() == null) {
			return null;
		}
		return (SysUserDto) request.getSession().getAttribute(ESessionDefine.CURRENT_USER.getCode());
	}
}
