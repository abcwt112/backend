package com.labofjet.system.annotation;

import org.springframework.cache.annotation.CacheEvict;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
@Inherited
public @interface RedisCacheEvicat {
	CacheEvict[] evict() default {};
}
