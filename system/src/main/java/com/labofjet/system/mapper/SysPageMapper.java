package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysPageDto;
import com.labofjet.system.dto.SysPageExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysPageMapper {
	int countByExample(SysPageExample example);

	int deleteByExample(SysPageExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysPageDto record);

	int insertSelective(SysPageDto record);

	List<SysPageDto> selectByExample(SysPageExample example);

	SysPageDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysPageDto record, @Param("example") SysPageExample example);

	int updateByExample(@Param("record") SysPageDto record, @Param("example") SysPageExample example);

	int updateByPrimaryKeySelective(SysPageDto record);

	int updateByPrimaryKey(SysPageDto record);

	//custom
	List<SysPageDto> selectList(Map map);
}