package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysPermissionDto;
import com.labofjet.system.dto.SysRoleDto;
import com.labofjet.system.dto.SysRolePermissionDto;
import com.labofjet.system.dto.SysRolePermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRolePermissionMapper {
	int countByExample(SysRolePermissionExample example);

	int deleteByExample(SysRolePermissionExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysRolePermissionDto record);

	int insertSelective(SysRolePermissionDto record);

	List<SysRolePermissionDto> selectByExample(SysRolePermissionExample example);

	SysRolePermissionDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysRolePermissionDto record, @Param("example") SysRolePermissionExample example);

	int updateByExample(@Param("record") SysRolePermissionDto record, @Param("example") SysRolePermissionExample example);

	int updateByPrimaryKeySelective(SysRolePermissionDto record);

	int updateByPrimaryKey(SysRolePermissionDto record);

	// custom
	int insertByPermission(SysPermissionDto record);

	int insertByRole(SysRoleDto record);

}