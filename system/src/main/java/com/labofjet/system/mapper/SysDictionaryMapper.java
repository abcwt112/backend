package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysDictionaryDto;
import com.labofjet.system.dto.SysDictionaryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysDictionaryMapper {
	int countByExample(SysDictionaryExample example);

	int deleteByExample(SysDictionaryExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysDictionaryDto record);

	int insertSelective(SysDictionaryDto record);

	List<SysDictionaryDto> selectByExample(SysDictionaryExample example);

	SysDictionaryDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysDictionaryDto record, @Param("example") SysDictionaryExample example);

	int updateByExample(@Param("record") SysDictionaryDto record, @Param("example") SysDictionaryExample example);

	int updateByPrimaryKeySelective(SysDictionaryDto record);

	int updateByPrimaryKey(SysDictionaryDto record);

	//custom
	List<SysDictionaryDto> selectList(Map map);
}