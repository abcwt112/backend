package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysDictionaryInfoDto;
import com.labofjet.system.dto.SysDictionaryInfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysDictionaryInfoMapper {
	int countByExample(SysDictionaryInfoExample example);

	int deleteByExample(SysDictionaryInfoExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysDictionaryInfoDto record);

	int insertSelective(SysDictionaryInfoDto record);

	List<SysDictionaryInfoDto> selectByExample(SysDictionaryInfoExample example);

	SysDictionaryInfoDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysDictionaryInfoDto record, @Param("example") SysDictionaryInfoExample example);

	int updateByExample(@Param("record") SysDictionaryInfoDto record, @Param("example") SysDictionaryInfoExample example);

	int updateByPrimaryKeySelective(SysDictionaryInfoDto record);

	int updateByPrimaryKey(SysDictionaryInfoDto record);

	//custom
	List<SysDictionaryInfoDto> selectList(Map map);
}