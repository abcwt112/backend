package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysCommonQueryDto;

import java.util.List;
import java.util.Map;

public interface SysCommonQueryMapper {
	public Map<String, Object> find(SysCommonQueryDto dto);

	public List<Map<String, Object>> findList(SysCommonQueryDto dto);
}
