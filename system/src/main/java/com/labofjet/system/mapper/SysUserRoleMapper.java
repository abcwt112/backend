package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysUserDto;
import com.labofjet.system.dto.SysUserRoleDto;
import com.labofjet.system.dto.SysUserRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysUserRoleMapper {
	int countByExample(SysUserRoleExample example);

	int deleteByExample(SysUserRoleExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysUserRoleDto record);

	int insertSelective(SysUserRoleDto record);

	List<SysUserRoleDto> selectByExample(SysUserRoleExample example);

	SysUserRoleDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysUserRoleDto record, @Param("example") SysUserRoleExample example);

	int updateByExample(@Param("record") SysUserRoleDto record, @Param("example") SysUserRoleExample example);

	int updateByPrimaryKeySelective(SysUserRoleDto record);

	int updateByPrimaryKey(SysUserRoleDto record);

	// custom
	int insertByUser(SysUserDto record);

	List<SysUserRoleDto> selectList(Map<String, Object> params);

}