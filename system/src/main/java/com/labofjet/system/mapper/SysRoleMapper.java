package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysRoleDto;
import com.labofjet.system.dto.SysRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysRoleMapper {
	int countByExample(SysRoleExample example);

	int deleteByExample(SysRoleExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysRoleDto record);

	int insertSelective(SysRoleDto record);

	List<SysRoleDto> selectByExample(SysRoleExample example);

	SysRoleDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysRoleDto record, @Param("example") SysRoleExample example);

	int updateByExample(@Param("record") SysRoleDto record, @Param("example") SysRoleExample example);

	int updateByPrimaryKeySelective(SysRoleDto record);

	int updateByPrimaryKey(SysRoleDto record);

	//custom
	List<SysRoleDto> selectList(Map map);
}