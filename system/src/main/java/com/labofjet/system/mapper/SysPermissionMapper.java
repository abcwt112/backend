package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysPermissionDto;
import com.labofjet.system.dto.SysPermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysPermissionMapper {
	int countByExample(SysPermissionExample example);

	int deleteByExample(SysPermissionExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(SysPermissionDto record);

	int insertSelective(SysPermissionDto record);

	List<SysPermissionDto> selectByExample(SysPermissionExample example);

	SysPermissionDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") SysPermissionDto record, @Param("example") SysPermissionExample example);

	int updateByExample(@Param("record") SysPermissionDto record, @Param("example") SysPermissionExample example);

	int updateByPrimaryKeySelective(SysPermissionDto record);

	int updateByPrimaryKey(SysPermissionDto record);

	//custom
	List<SysPermissionDto> selectList(Map map);
}