package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysMenuDto;
import com.labofjet.system.dto.SysMenuExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysMenuMapper {
    int countByExample(SysMenuExample example);

    int deleteByExample(SysMenuExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysMenuDto record);

    int insertSelective(SysMenuDto record);

    List<SysMenuDto> selectByExample(SysMenuExample example);

    SysMenuDto selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysMenuDto record, @Param("example") SysMenuExample example);

    int updateByExample(@Param("record") SysMenuDto record, @Param("example") SysMenuExample example);

    int updateByPrimaryKeySelective(SysMenuDto record);

    int updateByPrimaryKey(SysMenuDto record);

    //custom
    List<SysMenuDto> selectList(Map<String, Object> map);

	List<SysMenuDto> selectMenuList(Map<String, Object> map);
}