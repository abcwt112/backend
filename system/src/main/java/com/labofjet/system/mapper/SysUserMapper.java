package com.labofjet.system.mapper;

import com.labofjet.system.dto.SysUserDto;
import com.labofjet.system.dto.SysUserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysUserMapper {
    int countByExample(SysUserExample example);

    int deleteByExample(SysUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysUserDto record);

    int insertSelective(SysUserDto record);

    List<SysUserDto> selectByExample(SysUserExample example);

    SysUserDto selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysUserDto record, @Param("example") SysUserExample example);

    int updateByExample(@Param("record") SysUserDto record, @Param("example") SysUserExample example);

    int updateByPrimaryKeySelective(SysUserDto record);

    int updateByPrimaryKey(SysUserDto record);

    //custom
    List<SysUserDto> selectList(Map map);
}