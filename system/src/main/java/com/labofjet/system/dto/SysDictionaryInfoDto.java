package com.labofjet.system.dto;

import java.io.Serializable;

public class SysDictionaryInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String sn;

	private String text;

	private String state;

	private String psn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPsn() {
		return psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}
}