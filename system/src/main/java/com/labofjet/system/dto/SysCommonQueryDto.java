package com.labofjet.system.dto;

public class SysCommonQueryDto {
	private String tableName;
	private Integer id;
	private String text;
	private String iname;
	private String sn;
	private Integer orgId;
	private String columnName;
	private Object columnValue;
	private String where;

	public SysCommonQueryDto() {

	}

	public SysCommonQueryDto(Builder b) {
		tableName = b.tableName;
		id = b.id;
		iname = b.iname;
		sn = b.sn;
		columnName = b.columnName;
		columnValue = b.columnValue;
		orgId = b.orgId;
		where = b.where;
		text = b.text;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public Object getColumnValue() {
		return columnValue;
	}

	public void setColumnValue(Object columnValue) {
		this.columnValue = columnValue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIname() {
		return iname;
	}

	public void setIname(String iname) {
		this.iname = iname;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public static class Builder {
		private String tableName;
		private Integer id;
		private String iname;
		private String sn;
		private String text;
		private Integer orgId;
		private String columnName;
		private Object columnValue;
		private String where;

		public Builder tableName(String s) {
			tableName = s;
			return this;
		}

		public Builder text(String s) {
			text = s;
			return this;
		}

		public Builder id(Integer s) {
			id = s;
			return this;
		}

		public Builder iname(String s) {
			iname = s;
			return this;
		}

		public Builder sn(String s) {
			sn = s;
			return this;
		}

		public Builder orgId(Integer s) {
			orgId = s;
			return this;
		}

		public Builder columnName(String s) {
			columnName = s;
			return this;
		}

		public Builder columnValue(Object s) {
			columnValue = s;
			return this;
		}

		public Builder where(String s) {
			where = s;
			return this;
		}

		public SysCommonQueryDto build() {
			return new SysCommonQueryDto(this);
		}

		@Override
		public String toString() {
			return "Builder{" +
					"tableName='" + tableName + '\'' +
					", id=" + id +
					", text='" + text + '\'' +
					", iname='" + iname + '\'' +
					", sn='" + sn + '\'' +
					", orgId=" + orgId +
					", columnName='" + columnName + '\'' +
					", columnValue=" + columnValue +
					", where='" + where + '\'' +
					'}';
		}

	}

	@Override
	public String toString() {
		return "CommonQueryDto{" +
				"tableName='" + tableName + '\'' +
				", id=" + id +
				", text=" + text +
				", iname='" + iname + '\'' +
				", sn='" + sn + '\'' +
				", orgId=" + orgId +
				", columnName='" + columnName + '\'' +
				", columnValue=" + columnValue +
				", where='" + where + '\'' +
				'}';
	}

}
