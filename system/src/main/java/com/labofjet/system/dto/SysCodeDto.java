package com.labofjet.system.dto;

public class SysCodeDto {
	private String packageName;
	private String table;

	private String serviceImplCode;

	private String controllerCode;

	private String pageCode;

	private String mapperCode;

	private String mapperXMLCode;

	private String vuePageCode;

	private String className;

	private String javaName;

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getJavaName() {
		return javaName;
	}

	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getVuePageCode() {
		return vuePageCode;
	}

	public void setVuePageCode(String vuePageCode) {
		this.vuePageCode = vuePageCode;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getMapperCode() {
		return mapperCode;
	}

	public void setMapperCode(String mapperCode) {
		this.mapperCode = mapperCode;
	}

	public String getMapperXMLCode() {
		return mapperXMLCode;
	}

	public void setMapperXMLCode(String mapperXMLCode) {
		this.mapperXMLCode = mapperXMLCode;
	}

	public String getServiceImplCode() {
		return serviceImplCode;
	}

	public void setServiceImplCode(String serviceImplCode) {
		this.serviceImplCode = serviceImplCode;
	}

	public String getControllerCode() {
		return controllerCode;
	}

	public void setControllerCode(String controllerCode) {
		this.controllerCode = controllerCode;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	@Override
	public String toString() {
		return "SysCodeDto{" +
				"packageName='" + packageName + '\'' +
				", table='" + table + '\'' +
				", serviceImplCode='" + serviceImplCode + '\'' +
				", controllerCode='" + controllerCode + '\'' +
				", pageCode='" + pageCode + '\'' +
				", mapperCode='" + mapperCode + '\'' +
				", mapperXMLCode='" + mapperXMLCode + '\'' +
				", vuePageCode='" + vuePageCode + '\'' +
				", className='" + className + '\'' +
				", javaName='" + javaName + '\'' +
                ", url= '" + url + '\'' +
				'}';
	}
}