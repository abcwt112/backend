package com.labofjet.system.dto;

public class SysLoginDto {
	private String username;
	private String password;
	private String token;
	private String user;
	private Boolean rememberMe;
	private Integer expire;
	private SysUserDto sysUserDto;

	public Integer getExpire() {
		return expire;
	}

	public void setExpire(Integer expire) {
		this.expire = expire;
	}

	public SysUserDto getSysUserDto() {
		return sysUserDto;
	}

	public void setSysUserDto(SysUserDto sysUserDto) {
		this.sysUserDto = sysUserDto;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	@Override
	public String toString() {
		return "SysLoginDto{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				", token='" + token + '\'' +
				", user='" + user + '\'' +
				", rememberMe=" + rememberMe +
				", expire=" + expire +
				", sysUserDto=" + sysUserDto +
				'}';
	}
}
