package com.labofjet.system.dto;

import java.io.Serializable;
import java.util.List;

public class SysPermissionDto implements Serializable {

	private static final long serialVersionUID = 1L;

    private Integer id;

    private String sn;

    private String text;

    private String type;

    private String state;

	private List<SysRoleDto> sysRoleDtoList;

	private List<Integer> sysRoleIdList;

	public List<SysRoleDto> getSysRoleDtoList() {
		return sysRoleDtoList;
	}

	public void setSysRoleDtoList(List<SysRoleDto> sysRoleDtoList) {
		this.sysRoleDtoList = sysRoleDtoList;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

	public List<Integer> getSysRoleIdList() {
		return sysRoleIdList;
	}

	public void setSysRoleIdList(List<Integer> sysRoleIdList) {
		this.sysRoleIdList = sysRoleIdList;
	}

	@Override
	public String toString() {
		return "SysPermissionDto{" +
				"id=" + id +
				", sn='" + sn + '\'' +
				", text='" + text + '\'' +
				", type='" + type + '\'' +
				", state='" + state + '\'' +
				", sysRoleDtoList=" + sysRoleDtoList +
				", sysRoleIdList=" + sysRoleIdList +
				'}';
	}
}