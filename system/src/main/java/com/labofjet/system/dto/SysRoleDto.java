package com.labofjet.system.dto;

import java.io.Serializable;
import java.util.List;

public class SysRoleDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String sn;

	private String text;

	private List<SysPermissionDto> sysPermissionDtoList;

	private List<Integer> sysPermissionIdList;

	private String state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<SysPermissionDto> getSysPermissionDtoList() {
		return sysPermissionDtoList;
	}

	public void setSysPermissionDtoList(List<SysPermissionDto> sysPermissionDtoList) {
		this.sysPermissionDtoList = sysPermissionDtoList;
	}

	public List<Integer> getSysPermissionIdList() {
		return sysPermissionIdList;
	}

	public void setSysPermissionIdList(List<Integer> sysPermissionIdList) {
		this.sysPermissionIdList = sysPermissionIdList;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "SysRoleDto{" +
				"id=" + id +
				", sn='" + sn + '\'' +
				", text='" + text + '\'' +
				", sysPermissionDtoList=" + sysPermissionDtoList +
				", sysPermissionIdList=" + sysPermissionIdList +
				", state='" + state + '\'' +
				'}';
	}
}