package com.labofjet.system.dto;

import java.io.Serializable;

public class SysMenuDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String value;

	private Integer pid;

	private String state;

	private String icon;

	private String name;

	private Integer sort;

	private String parentName;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		return "SysMenuDto{" +
				"id=" + id +
				", value='" + value + '\'' +
				", pid=" + pid +
				", state='" + state + '\'' +
				", icon='" + icon + '\'' +
				", name='" + name + '\'' +
				", sort=" + sort +
				", parentName='" + parentName + '\'' +
				'}';
	}
}