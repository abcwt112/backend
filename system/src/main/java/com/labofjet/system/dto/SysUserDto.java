package com.labofjet.system.dto;

import java.io.Serializable;
import java.util.List;

public class SysUserDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String username;

    private String password;

    private String email;

    private String state;

    private List<SysPermissionDto> sysPermissionDtos;

    private List<Integer> sysRoleIdList;

    private List<SysRoleDto> sysRoleDtoList;

    public List<SysPermissionDto> getSysPermissionDtos() {
        return sysPermissionDtos;
    }

    private String phoneNumber;

    public void setSysPermissionDtos(List<SysPermissionDto> sysPermissionDtos) {
        this.sysPermissionDtos = sysPermissionDtos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Integer> getSysRoleIdList() {
        return sysRoleIdList;
    }

    public void setSysRoleIdList(List<Integer> sysRoleIdList) {
        this.sysRoleIdList = sysRoleIdList;
    }

    public List<SysRoleDto> getSysRoleDtoList() {
        return sysRoleDtoList;
    }

    public void setSysRoleDtoList(List<SysRoleDto> sysRoleDtoList) {
        this.sysRoleDtoList = sysRoleDtoList;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "SysUserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", state='" + state + '\'' +
                ", sysPermissionDtos=" + sysPermissionDtos +
                ", sysRoleIdList=" + sysRoleIdList +
                ", sysRoleDtoList=" + sysRoleDtoList +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}