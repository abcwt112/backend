package com.labofjet.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.system.annotation.RedisCacheEvicat;
import com.labofjet.system.constant.CRedisCacheConstant;
import com.labofjet.system.dto.SysRoleDto;
import com.labofjet.system.dto.SysRoleExample;
import com.labofjet.system.dto.SysRolePermissionExample;
import com.labofjet.system.mapper.SysRoleMapper;
import com.labofjet.system.mapper.SysRolePermissionMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class SysRoleServiceImpl extends CrudBaseServiceImpl<SysRoleDto> {
    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Autowired
    private SysPermissionServiceImpl sysPermissionService;

    @Override
    @Cacheable(value = CRedisCacheConstant.ROLE_PAGE_PREFIX, key = CRedisCacheConstant.CACHE_PAGE)
    public PageInfo<SysRoleDto> list(ContextDto result) {
        Map<String, Object> map = super.prepare(result);
        List<SysRoleDto> SysRoleDtos = sysRoleMapper.selectList(map);
        return new PageInfo<>(SysRoleDtos);
    }

    @Override
    @Cacheable(value = CRedisCacheConstant.ROLE_ID_PREFIX, key = CRedisCacheConstant.CACHE_ID)
    public SysRoleDto load(ContextDto result) {
        Map<String, Object> map = super.getRequestParamsMap(result);
        Integer id = (Integer) map.get("id");
        if (null != id) {
            SysRoleDto sysRoleDto = sysRoleMapper.selectList(map).get(0);
            return sysRoleDto;
        }
        return null;
    }

    @Override
    @Transactional
    @Caching(evict = {
            @CacheEvict(value = CRedisCacheConstant.ROLE_ID_PREFIX, key = CRedisCacheConstant.CACHE_EVICT_ID)
    })
    @RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.ROLE_PAGE_PREFIX, allEntries = true))
    public SysRoleDto save(ContextDto result) {
        SysRoleDto dto = JSONUtils.getRequestData(result, SysRoleDto.class);
        if (dto.getId() != null) {
            sysRoleMapper.updateByPrimaryKey(dto);
        } else {
            sysRoleMapper.insertSelective(dto);
        }
        this.saveRolePermission(dto);
        sysPermissionService.evictPageCache();
        return dto;
    }

    @Override
    @Transactional
    @RedisCacheEvicat(evict = {@CacheEvict(value = CRedisCacheConstant.ROLE_PAGE_PREFIX, allEntries = true),
            @CacheEvict(value = CRedisCacheConstant.ROLE_ID_PREFIX)})
    public int delete(ContextDto result) {
        List<Integer> ids = JSONUtils.getRequestData(result, List.class);
        SysRoleExample example = new SysRoleExample();
        example.createCriteria().andIdIn(ids);
        int i = sysRoleMapper.deleteByExample(example);
        return i;
    }

    /**
     * 保存角色关联的权限
     *
     * @param dto
     */
    private void saveRolePermission(SysRoleDto dto) {
        List<Integer> ids = dto.getSysPermissionIdList();
        if (CollectionUtils.isNotEmpty(ids)) {
            SysRolePermissionExample example = new SysRolePermissionExample();
            example.createCriteria().andPermissionIdNotIn(ids).andRoleIdEqualTo(dto.getId());
            sysRolePermissionMapper.deleteByExample(example);
            sysRolePermissionMapper.insertByRole(dto);
        } else {
            SysRolePermissionExample example = new SysRolePermissionExample();
            example.createCriteria().andRoleIdEqualTo(dto.getId());
            sysRolePermissionMapper.deleteByExample(example);
        }
    }

    /**
     * 清除page缓存
     */
    @RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.ROLE_PAGE_PREFIX, allEntries = true))
    public void evictPageCache() {
    }

    public List<SysRoleDto> find(SysRoleDto sysRoleDto) {
        SysRoleExample sysRoleExample = new SysRoleExample();
        SysRoleExample.Criteria criteria = sysRoleExample.createCriteria();
        if (StringUtils.isNotEmpty(sysRoleDto.getSn())) {
            criteria.andSnEqualTo(sysRoleDto.getSn());
        }

        return sysRoleMapper.selectByExample(sysRoleExample);
    }
}
