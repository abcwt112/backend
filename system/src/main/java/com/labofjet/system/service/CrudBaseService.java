package com.labofjet.system.service;


import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;

public interface CrudBaseService<T> {

	PageInfo<T> list(ContextDto result);

	T load(ContextDto result);

	T save(ContextDto result);

	int delete(ContextDto result);
}
