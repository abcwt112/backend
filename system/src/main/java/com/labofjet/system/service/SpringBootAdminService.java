package com.labofjet.system.service;


import feign.Headers;
import feign.Param;
import feign.RequestLine;

//@FeignClient(value = "springBootAdminService", url = "http://localhost/springbootadmin")
public interface SpringBootAdminService {

	//	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@Headers("JSESSIONID: {jsid}")
	@RequestLine("POST /login")
	String login(String body, @Param("jsid") String jsid);
}
