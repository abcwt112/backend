package com.labofjet.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.system.annotation.RedisCacheEvicat;
import com.labofjet.system.constant.CRedisCacheConstant;
import com.labofjet.system.dto.SysDictionaryInfoDto;
import com.labofjet.system.dto.SysDictionaryInfoExample;
import com.labofjet.system.mapper.SysDictionaryInfoMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 字典详情  serviceImpl类
 */
@Service
public class SysDictionaryInfoServiceImpl extends CrudBaseServiceImpl<SysDictionaryInfoDto> {
	@Resource
	private SysDictionaryInfoMapper sysDictionaryInfoMapper;

	@Override
	@Cacheable(value = CRedisCacheConstant.DIC_INFO_PAGE_PREFIX, key = CRedisCacheConstant.CACHE_PAGE)
	public PageInfo<SysDictionaryInfoDto> list(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<SysDictionaryInfoDto> SysDictionaryInfoDtos = sysDictionaryInfoMapper.selectList(map);
		return new PageInfo<>(SysDictionaryInfoDtos);
	}

	@Override
	@Cacheable(value = CRedisCacheConstant.DIC_INFO_ID_PREFIX, key = CRedisCacheConstant.CACHE_ID)
	public SysDictionaryInfoDto load(ContextDto result) {
		Map<String, Object> map = super.getRequestParamsMap(result);
		Integer id = (Integer) map.get("id");
		if (null != id) {
			return sysDictionaryInfoMapper.selectList(map).get(0);
		}
		return null;
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CRedisCacheConstant.DIC_INFO_ID_PREFIX, key = CRedisCacheConstant.CACHE_EVICT_ID)
	})
	@RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.DIC_INFO_PAGE_PREFIX, allEntries = true))
	public SysDictionaryInfoDto save(ContextDto result) {
		SysDictionaryInfoDto dto = JSONUtils.getRequestData(result, SysDictionaryInfoDto.class);
		if (dto.getId() != null) {
			sysDictionaryInfoMapper.updateByPrimaryKey(dto);
		} else {
			sysDictionaryInfoMapper.insertSelective(dto);
		}
		return dto;
	}

	@Override
	@Transactional
	@RedisCacheEvicat(evict = {@CacheEvict(value = CRedisCacheConstant.DIC_INFO_PAGE_PREFIX, allEntries = true),
			@CacheEvict(value = CRedisCacheConstant.DIC_INFO_ID_PREFIX)})
	public int delete(ContextDto result) {
		List<Integer> ids = JSONUtils.getRequestData(result, List.class);
		SysDictionaryInfoExample example = new SysDictionaryInfoExample();
		example.createCriteria().andIdIn(ids);
		int i = sysDictionaryInfoMapper.deleteByExample(example);
		return i;
	}
}
