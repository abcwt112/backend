package com.labofjet.system.service;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.log.annotation.NoAopLog;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@NoAopLog
@FeignClient(value = "log-collector-mq")
public interface SysLogService {

	@RequestMapping(value = "/log/list", method = RequestMethod.POST)
	ContextDto
	list(ContextDto result);

	@RequestMapping(value = "/log/load", method = RequestMethod.POST)
	ContextDto load(ContextDto result);
}
