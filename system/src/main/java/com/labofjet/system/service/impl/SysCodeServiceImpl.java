package com.labofjet.system.service.impl;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.system.dto.SysCodeDto;
import com.labofjet.system.dto.SysMenuDto;
import com.labofjet.system.generator.context.TaskContext;
import com.labofjet.system.generator.enums.ETaskDefine;
import com.labofjet.system.generator.task.Task;
import com.labofjet.system.generator.task.impl.TaskGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SysCodeServiceImpl extends CrudBaseServiceImpl<SysMenuDto> {

	@Autowired
	@Qualifier("DBInitTask")
	private Task initTask;

	@Autowired
	@Qualifier("serviceImplGenerateTask")
	private Task serviceTask;

	@Autowired
	@Qualifier("controllerGenerateTask")
	private Task controllerTask;

	@Autowired
	@Qualifier("mapperGenerateTask")
	private Task mapperTask;

	@Autowired
	@Qualifier("mapperXMLGenerateTask")
	private Task mapperXMLTask;

	@Autowired
	@Qualifier("vuePageGenerateTask")
	private Task vuePageTask;


	/**
	 * 生成代码
	 *
	 * @return
	 */
	public SysCodeDto generateCode(ContextDto c) {
		SysCodeDto codeDto = JSONUtils.getRequestData(c, SysCodeDto.class);

		TaskContext context = new TaskContext();
		context.put(ETaskDefine.PACKAGE_NAME.getCode(), codeDto.getPackageName());
		context.put(ETaskDefine.TABLE.getCode(), codeDto.getTable());
		context.put(ETaskDefine.RESULT.getCode(), codeDto);
		Task task = new TaskGroup(initTask, mapperTask, mapperXMLTask, serviceTask, controllerTask, vuePageTask); // 要注意顺序
		task.doTask(context);

		return codeDto;
	}


}
