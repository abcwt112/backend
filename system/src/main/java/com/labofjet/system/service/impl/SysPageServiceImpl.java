package com.labofjet.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.system.annotation.RedisCacheEvicat;
import com.labofjet.system.constant.CRedisCacheConstant;
import com.labofjet.system.dto.SysPageDto;
import com.labofjet.system.dto.SysPageExample;
import com.labofjet.system.mapper.SysPageMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class SysPageServiceImpl extends CrudBaseServiceImpl<SysPageDto> {
	@Resource
	private SysPageMapper sysPageMapper;

	public List<SysPageDto> listPage(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<SysPageDto> sysPageDtos = sysPageMapper.selectList(map);
		return sysPageDtos;
	}

	@Override
	@Cacheable(value = CRedisCacheConstant.PAGE_PAGE_PREFIX, key = CRedisCacheConstant.CACHE_PAGE)
	public PageInfo<SysPageDto> list(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<SysPageDto> SysPageDtos = sysPageMapper.selectList(map);
		return new PageInfo<>(SysPageDtos);
	}

	@Override
	@Cacheable(value = CRedisCacheConstant.PAGE_ID_PREFIX, key = CRedisCacheConstant.CACHE_ID)
	public SysPageDto load(ContextDto result) {
		Map<String, Object> map = super.getRequestParamsMap(result);
		Integer id = (Integer) map.get("id");
		if (null != id) {
			return sysPageMapper.selectList(map).get(0);
		}
		return null;
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CRedisCacheConstant.PAGE_ID_PREFIX, key = CRedisCacheConstant.CACHE_EVICT_ID)
	})
	@RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.PAGE_PAGE_PREFIX, allEntries = true))
	public SysPageDto save(ContextDto result) {
		SysPageDto dto = JSONUtils.getRequestData(result, SysPageDto.class);
		if (dto.getId() != null) {
			sysPageMapper.updateByPrimaryKey(dto);
		} else {
			sysPageMapper.insertSelective(dto);
		}
		return dto;
	}

	@Override
	@Transactional
	@RedisCacheEvicat(evict = {@CacheEvict(value = CRedisCacheConstant.PAGE_PAGE_PREFIX, allEntries = true),
			@CacheEvict(value = CRedisCacheConstant.PAGE_ID_PREFIX)})
	public int delete(ContextDto result) {
		List<Integer> ids = JSONUtils.getRequestData(result, List.class);
		SysPageExample example = new SysPageExample();
		example.createCriteria().andIdIn(ids);
		int i = sysPageMapper.deleteByExample(example);
		return i;
	}
}
