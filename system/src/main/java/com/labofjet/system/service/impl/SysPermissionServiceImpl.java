package com.labofjet.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.system.annotation.RedisCacheEvicat;
import com.labofjet.system.constant.CRedisCacheConstant;
import com.labofjet.system.dto.SysPermissionDto;
import com.labofjet.system.dto.SysPermissionExample;
import com.labofjet.system.dto.SysRolePermissionExample;
import com.labofjet.system.mapper.SysPermissionMapper;
import com.labofjet.system.mapper.SysRolePermissionMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
@Service
public class SysPermissionServiceImpl extends CrudBaseServiceImpl<SysPermissionDto> {
	@Resource
	private SysPermissionMapper sysPermissionMapper;

	@Resource
	private SysRolePermissionMapper sysRolePermissionMapper;

	@Autowired
	private SysRoleServiceImpl sysRoleService;



	@Override
	@Cacheable(value = CRedisCacheConstant.PERMISSION_PAGE_PREFIX, key = CRedisCacheConstant.CACHE_PAGE)
	public PageInfo<SysPermissionDto> list(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<SysPermissionDto> SysPermissionDtos = sysPermissionMapper.selectList(map);
		return new PageInfo<>(SysPermissionDtos);
	}

	@Override
	@Cacheable(value = CRedisCacheConstant.PERMISSION_ID_PREFIX, key = CRedisCacheConstant.CACHE_ID)
	public SysPermissionDto load(ContextDto result) {
		Map<String, Object> map = super.getRequestParamsMap(result);
		Integer id = (Integer) map.get("id");
		if (null != id) {
			SysPermissionDto sysPermissionDto = sysPermissionMapper.selectList(map).get(0);
			return sysPermissionDto;
		}
		return null;
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CRedisCacheConstant.PERMISSION_ID_PREFIX, key = CRedisCacheConstant.CACHE_EVICT_ID)
	})
	@RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.PERMISSION_PAGE_PREFIX, allEntries = true))
	public SysPermissionDto save(ContextDto result) {
		SysPermissionDto dto = JSONUtils.getRequestData(result, SysPermissionDto.class);
		if (dto.getId() != null) {
			sysPermissionMapper.updateByPrimaryKey(dto);
		} else {
			sysPermissionMapper.insertSelective(dto);
		}
		this.saveRolePermission(dto);
		sysRoleService.evictPageCache();
		return dto;
	}

	@Override
	@Transactional
	@RedisCacheEvicat(evict = {@CacheEvict(value = CRedisCacheConstant.PERMISSION_PAGE_PREFIX, allEntries = true),
			@CacheEvict(value = CRedisCacheConstant.PERMISSION_ID_PREFIX)})
	public int delete(ContextDto result) {
		List<Integer> ids = JSONUtils.getRequestData(result, List.class);
		SysPermissionExample example = new SysPermissionExample();
		example.createCriteria().andIdIn(ids);
		int i = sysPermissionMapper.deleteByExample(example);
		return i;
	}

	/**
	 * 保存关联的角色
	 * @param dto
	 */
	private void saveRolePermission(SysPermissionDto dto) {
		List<Integer> ids = dto.getSysRoleIdList();
		if (CollectionUtils.isNotEmpty(ids)) {
			SysRolePermissionExample example = new SysRolePermissionExample();
			example.createCriteria().andRoleIdNotIn(ids).andPermissionIdEqualTo(dto.getId());
			sysRolePermissionMapper.deleteByExample(example);
			sysRolePermissionMapper.insertByPermission(dto);
		} else {
			SysRolePermissionExample example = new SysRolePermissionExample();
			example.createCriteria().andPermissionIdEqualTo(dto.getId());
			sysRolePermissionMapper.deleteByExample(example);
		}
	}

	/**
	 * 清除page缓存
	 */
	@RedisCacheEvicat(evict = @CacheEvict(value = CRedisCacheConstant.PERMISSION_PAGE_PREFIX, allEntries = true))
	public void evictPageCache() {
	}

}
