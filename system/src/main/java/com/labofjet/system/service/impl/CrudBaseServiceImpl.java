package com.labofjet.system.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.service.impl.BaseServiceImpl;
import com.labofjet.system.dto.SysUserDto;
import com.labofjet.system.enums.ESessionDefine;
import com.labofjet.system.service.CrudBaseService;
import com.labofjet.system.util.SysCommonUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class CrudBaseServiceImpl<T> extends BaseServiceImpl implements CrudBaseService<T> {
    protected Logger log = LoggerFactory.getLogger(this.getClass());


    /**
     * 注入session里的必备数据
     *
     * @return
     */
    public void injectSession(Map<String, Object> map) {
        if (MapUtils.isNotEmpty(map)) {
            SysUserDto currentUser = SysCommonUtils.getCurrentUser();
            map.put(ESessionDefine.CURRENT_USER.getCode(), currentUser); // 登陆的用户
            if(currentUser != null) {
                map.put("isAdmin", currentUser.getSysRoleIdList().contains(1));
            }
        }
    }

    /**
     * 获取请求参数,浅复制
     *
     * @param contextDto
     * @return
     */
    public Map<String, Object> getRequestParamsMap(ContextDto contextDto) {
        if (MapUtils.isEmpty(contextDto.getRequestParams())) {
            return new HashMap<>();
        }
        return new HashMap<>(contextDto.getRequestParams());
    }

    /**
     * 设置分页参数和排序参数
     */
    public void handlePageAndOrder(Map<String, Object> map) {
        if (MapUtils.isNotEmpty(map)) {
            // pageNum=1&pageSize=10&count=true
            if (map.get("pageNum") != null) {
                PageHelper.startPage(map);
            }
            if (map.get("sortName") != null && StringUtils.isNotBlank(map.get("sortName").toString())) {
                StringBuilder b = new StringBuilder();
                List<String> sortName = Arrays.asList(map.get("sortName").toString().split(","));
                List<String> sortOrder = Arrays.asList(map.get("sortOrder").toString().split(","));
                for (int i = 0; i < sortName.size(); i++) {
                    b.append(" ").append(sortName.get(i)).append(" ").append(sortOrder.get(i)).append(",");
                }
                PageHelper.orderBy(b.toString().substring(0, b.length() - 1));
            }
        }
    }

    /**
     * 设置分页参数和排序并且获取参数map并且设置session里的一些参数
     *
     * @param contextDto
     * @return
     */
    public Map<String, Object> prepare(ContextDto contextDto) {
        Map<String, Object> requestParamsMap = this.getRequestParamsMap(contextDto);
        this.handlePageAndOrder(requestParamsMap);
        this.injectSession(requestParamsMap);
        return requestParamsMap;
    }

    @Override
    public PageInfo<T> list(ContextDto result) {
        throw new IllegalStateException("不支持的方法调用 list");
    }

    @Override
    public T load(ContextDto result) {
        throw new IllegalStateException("不支持的方法调用 load");
    }

    @Override
    public T save(ContextDto result) {
        throw new IllegalStateException("不支持的方法调用 save");
    }

    @Override
    public int delete(ContextDto result) {
        throw new IllegalStateException("不支持的方法调用 delete");
    }
}
