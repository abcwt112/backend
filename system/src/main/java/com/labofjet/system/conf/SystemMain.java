package com.labofjet.system.conf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.labofjet")
@ServletComponentScan(basePackages = "com.labofjet")
@MapperScan("com.labofjet")
@EnableAutoConfiguration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60 * 60)
@EnableTransactionManagement(proxyTargetClass = true)
@EnableFeignClients(basePackages = "com.labofjet")
@EnableCaching
@EnableEurekaClient
public class SystemMain {

	public static void main(String[] args) {
		SpringApplication.run(SystemMain.class, args);
	}

}