package com.labofjet.system.constant;

public interface CRedisCacheConstant {

	// scripe path
	String SCRIPT_PATH = "redis/script";

	// 共用部分,注意方法参数名
	String CACHE_PAGE = "T(com.labofjet.system.util.RedisCacheUtils).calcKey(#result)";
	String CACHE_ID = "T(com.labofjet.system.util.RedisCacheUtils).calcIdKey(#result)";
	String CACHE_EVICT_ID = "T(com.labofjet.system.util.RedisCacheUtils).evictIdKey(#result.id)";

	// dicinfo
	String DIC_INFO_PAGE_PREFIX = "system:cache:dicInfo:page";
	String DIC_INFO_ID_PREFIX = "system:cache:dicInfo:id";

	//dic
	String DIC_PAGE_PREFIX = "system:cache:dic:page";
	String DIC_ID_PREFIX = "system:cache:dic:id";

	//menu
	String MENU_PAGE_PREFIX = "system:cache:menu:page";
	String MENU_ID_PREFIX = "system:cache:menu:id";

	//page
	String PAGE_PAGE_PREFIX = "system:cache:page:page";
	String PAGE_ID_PREFIX = "system:cache:page:id";

	//permission
	String PERMISSION_PAGE_PREFIX = "system:cache:permission:page";
	String PERMISSION_ID_PREFIX = "system:cache:permission:id";

	//role
	String ROLE_PAGE_PREFIX = "system:cache:role:page";
	String ROLE_ID_PREFIX = "system:cache:role:id";

	//user
	String USER_PAGE_PREFIX = "system:cache:user:page";
	String USER_ID_PREFIX = "system:cache:user:id";

}

