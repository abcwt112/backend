package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum ERedisDefine implements BaseEnum {
	TOKEN_PREFIX("system:user:token:", "system:user:token:"),

	USER_LOGIN_FAIL_TIMES("system:user:loginFailTimes:","system:user:loginFailTimes:") ;

	private String code;
	private String text;

	ERedisDefine(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
