package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum ESessionDefine implements BaseEnum {
	CURRENT_USER("user", "user");

	private String code;
	private String text;

	ESessionDefine(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
