package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EResult implements BaseEnum {
	SUCC("1", "成功"), FAIL("2", "失败");

	private String code;
	private String text;

	EResult(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
