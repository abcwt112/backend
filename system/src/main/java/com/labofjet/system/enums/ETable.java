package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum ETable implements BaseEnum {
	DIC("sys_dictionary", "sys_dictionary"),
	DIC_INFO("sys_dictionary_info", "sys_dictionary_info");

	private String code;
	private String text;

	ETable(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}
}
