package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EColumn implements BaseEnum {
	SN("sn", "sn"),
	TEXT("text", "text"),
	PSN("psn", "psn");

	private String code;
	private String text;

	EColumn(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}
}
