package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EState implements BaseEnum {
	VALID("1", "有效"), INVALID("2", "无效");

	private String code;
	private String text;

	EState(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
