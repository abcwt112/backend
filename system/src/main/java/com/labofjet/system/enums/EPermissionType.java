package com.labofjet.system.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EPermissionType implements BaseEnum {
	URL("url", "url"), MENU("menu", "menu");

	private String code;
	private String text;

	EPermissionType(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
