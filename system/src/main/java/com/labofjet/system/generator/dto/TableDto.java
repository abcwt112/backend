package com.labofjet.system.generator.dto;

public class TableDto {
	private String comment;


	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "FieldDto{" +
				", comment='" + comment + '\'' +
				'}';
	}
}
