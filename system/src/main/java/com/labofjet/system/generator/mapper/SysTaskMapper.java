package com.labofjet.system.generator.mapper;

import com.labofjet.system.generator.dto.FieldDto;
import com.labofjet.system.generator.dto.TableDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysTaskMapper {
	List<FieldDto> getFieldInfo(Map<String, Object> map);

	TableDto getTableInfo(Map<String, Object> map);
}