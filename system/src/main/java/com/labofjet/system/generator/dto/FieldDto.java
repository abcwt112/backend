package com.labofjet.system.generator.dto;

public class FieldDto {
	private String field;
	private String comment;
	private String javaName;

	public String getJavaName() {
		return javaName;
	}

	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "FieldDto{" +
				"field='" + field + '\'' +
				", comment='" + comment + '\'' +
				", javaName='" + javaName + '\'' +
				'}';
	}
}
