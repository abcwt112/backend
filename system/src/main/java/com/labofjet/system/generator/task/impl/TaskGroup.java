package com.labofjet.system.generator.task.impl;

import com.labofjet.system.generator.context.TaskContext;
import com.labofjet.system.generator.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskGroup implements Task {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskGroup.class);

	private List<Task> tasks = new ArrayList<>();

	public TaskGroup() {

	}

	public TaskGroup(Task... t) {
		tasks.addAll(Arrays.asList(t));
	}


	@Override
	public boolean doTask(TaskContext c) {
		LOGGER.debug("开始执行TaskGroup " + this.getName());
		for (Task task : tasks) { // 任何任务任意环节出错,就停止后续任务(之前的不回滚)...暂时不支持失败继续下个任务
			Boolean b = null;
			LOGGER.debug("开始执行任务 " + task.getName());
			b = task.beforeTask(c);
			LOGGER.debug("beforeTask 执行结果成功?  " + b);
			if (!b) {
				return false;
			}
			b = task.doTask(c);
			LOGGER.debug("doTask 执行结果成功?  " + b);
			if (!b) {
				return false;
			}
			b = task.afterTask(c);
			LOGGER.debug("afterTask 执行结果成功?  " + b);
			if (!b) {
				return false;
			}
		}
		LOGGER.debug("TaskGroup执行完毕 " + this.getName());
		return true;
	}


}
