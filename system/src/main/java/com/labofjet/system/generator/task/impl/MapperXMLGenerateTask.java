package com.labofjet.system.generator.task.impl;

import com.labofjet.system.dto.SysCodeDto;
import com.labofjet.system.generator.context.TaskContext;
import com.labofjet.system.generator.dto.FieldDto;
import com.labofjet.system.generator.enums.ETaskDefine;
import com.labofjet.system.generator.task.FileStringGeneratorTask;
import com.labofjet.system.generator.util.GeneratorUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * service任务
 */
@Component
public class MapperXMLGenerateTask extends FileStringGeneratorTask {

	@Override
	protected String getTemplateRelativePath() {
		return "MapperXML.ftl";
	}

	@Override
	protected void afterFileProcess(TaskContext c, String result) {
		SysCodeDto sysCodeDto = c.get(ETaskDefine.RESULT.getCode());
		sysCodeDto.setMapperXMLCode(result);
	}

	@Override
	public boolean beforeTask(TaskContext c) {
		StringBuilder baseColumns = new StringBuilder(128);
		List<FieldDto> fs = c.get(ETaskDefine.FIELDS_INFO.getCode());
		for (FieldDto f : fs) {
			baseColumns.append(" ").append(f.getField()).append(" as ")
					.append(GeneratorUtils.underlineNameToJavaName(f.getField())).append(" ,");
		}
		String bc = StringUtils.removeEnd(baseColumns.toString(), ",");
		c.put(ETaskDefine.BASE_COLUMNS.getCode(), bc);
		return true;
	}
}
