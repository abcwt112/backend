package com.labofjet.system.generator.task.impl;

import com.labofjet.system.dto.SysCodeDto;
import com.labofjet.system.generator.context.TaskContext;
import com.labofjet.system.generator.enums.ETaskDefine;
import com.labofjet.system.generator.task.FileStringGeneratorTask;
import org.springframework.stereotype.Component;

/**
 * mapper任务
 */
@Component
public class VuePageGenerateTask extends FileStringGeneratorTask {
	
	@Override
	protected String getTemplateRelativePath() {
		return "VuePage.ftl";
	}

	@Override
	protected void afterFileProcess(TaskContext c, String result) {
		SysCodeDto sysCodeDto = c.get(ETaskDefine.RESULT.getCode());
		sysCodeDto.setVuePageCode(result);
	}


}
