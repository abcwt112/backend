package com.labofjet.system.generator.context;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public class TaskContext {
	private Map<String, Object> context = new HashMap<>();

	public <T> T put(String s, T o) {
		return (T) context.put(s, o);
	}

	public <T> T get(String s) {
		return (T) context.get(s);
	}

	public Map<String, Object> getContext() {
		return context;
	}
}
