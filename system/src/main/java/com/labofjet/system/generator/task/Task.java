package com.labofjet.system.generator.task;

import com.labofjet.system.generator.context.TaskContext;

/**
 * 任务
 */
public interface Task {
	default boolean beforeTask(TaskContext c) {
		return true;
	}

	default boolean doTask(TaskContext c) {
		return true;
	}

	default boolean afterTask(TaskContext c) {
		return true;
	}

	default String getName() {
		return this.getClass().getSimpleName();
	}
}
