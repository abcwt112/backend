package com.labofjet.system.generator.task.impl;

import com.labofjet.system.dto.SysCodeDto;
import com.labofjet.system.generator.context.TaskContext;
import com.labofjet.system.generator.enums.ETaskDefine;
import com.labofjet.system.generator.task.FileStringGeneratorTask;
import org.springframework.stereotype.Component;

/**
 * service任务
 */
@Component
public class ControllerGenerateTask extends FileStringGeneratorTask {

	@Override
	protected String getTemplateRelativePath() {
		return "Controller.ftl";
	}

	@Override
	protected void afterFileProcess(TaskContext c, String result) {
		SysCodeDto sysCodeDto = c.get(ETaskDefine.RESULT.getCode());
		sysCodeDto.setControllerCode(result);
	}

	@Override
	public boolean beforeTask(TaskContext c) {
		String table = c.get(ETaskDefine.TABLE.getCode());
		int index = table.indexOf("_");
		String result = table.substring(index + 1, table.length()).replaceAll("_", "").toLowerCase();
		c.put(ETaskDefine.URL.getCode(), result);
		return true;
	}
}
