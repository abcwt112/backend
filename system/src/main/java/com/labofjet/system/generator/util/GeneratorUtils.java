package com.labofjet.system.generator.util;

public class GeneratorUtils {

	/**
	 * _小写幻化成大写,首尾不能是_
	 * sys_menu_haha -> sysMenuHaha
	 *
	 * @param t
	 * @return
	 */
	public static String underlineNameToJavaName(String t) {
		while (t.contains("_")) {
			t = doTableNameToJavaNameInternal(t);
		}
		return t;
	}

	/**
	 * _小写幻化成大写,首字母大写,首尾不能是_
	 * sys_menu_haha -> SysMenuHaha
	 *
	 * @param t
	 * @return
	 */
	public static String underlineNameToJavaNameAndUpCaseFirstLetter(String t) {
		String temp = underlineNameToJavaName(t);
		return temp.substring(0, 1).toUpperCase() + temp.substring(1, temp.length());
	}


	private static String doTableNameToJavaNameInternal(String t) {
		int i = t.indexOf("_");
		String temp = String.valueOf(t.charAt(i + 1)).toUpperCase();
		String temp2 = t.substring(0, i) + temp + t.substring(i + 2, t.length());
		t = temp2;
		return t;
	}

	public static void main(String[] args) {
		System.out.println(underlineNameToJavaNameAndUpCaseFirstLetter("sys_menu_haha"));
	}

}
