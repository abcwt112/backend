package com.labofjet.system.generator.enums;

import com.labofjet.common.enums.BaseEnum;

public enum ETaskDefine implements BaseEnum {
	/**
	 * sys_menu
	 */
	TABLE("table", "table"),
	/**
	 * 数据库的field信息
	 */
	FIELDS_INFO("fieldsInfo", "fieldsInfo"),
	/**
	 * 数据库的table信息
	 */
	TABLE_INFO("tableInfo", "tableInfo"),
	/**
	 * com.labofjet.system
	 */
	PACKAGE_NAME("packageName", "packageName"),
	/**
	 * SysMenu
	 */
	CLASSNAME_SHORT("class", "class"),
	/**
	 * sysMenu
	 */
	VARIABLE("variable", "variable"),
	/**
	 * menu
	 */
	URL("url", "url"),
	/**
	 * id, name, ....
	 */
	BASE_COLUMNS("baseColumns", "baseColumns"),
	/**
	 * 处理结果
	 */
	RESULT("result", "result");

	private String code;
	private String text;

	ETaskDefine(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
