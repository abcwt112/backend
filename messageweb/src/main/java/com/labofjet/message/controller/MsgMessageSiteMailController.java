package com.labofjet.message.controller;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.message.service.MsgMessageSiteMailService;
import com.labofjet.system.controller.CrudBaseController;
import com.labofjet.system.util.SysCommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站内信  controller类
 */
@RestController
public class MsgMessageSiteMailController extends CrudBaseController {

	@Autowired
	private MsgMessageSiteMailService msgMessageSiteMailService;

	@RequestMapping("/messagesitemail/count")
	public ContextDto count(@RequestBody ContextDto contextDto) {
		contextDto.getRequestParams().put("receiverId", SysCommonUtils.getCurrentUser().getId());
		ContextDto result = msgMessageSiteMailService.count(contextDto);
		return result;
	}

	@RequestMapping("/messagesitemail/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		contextDto.getRequestParams().put("receiverId", SysCommonUtils.getCurrentUser().getId());
		ContextDto result = msgMessageSiteMailService.list(contextDto);
		return result;
	}

	@RequestMapping("/messagesitemail/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		ContextDto result = msgMessageSiteMailService.load(contextDto);
		return result;
	}

	@RequestMapping("/messagesitemail/readUnRead")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		ContextDto result = msgMessageSiteMailService.readUnRead(contextDto);
		return result;
	}

}
