package com.labofjet.message.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.message.service.impl.MsgMessageSenderServiceImpl;
import com.labofjet.system.controller.CrudBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息  controller类
 */
@RestController
public class MsgMessageSenderController extends CrudBaseController {

	@Autowired
	private MsgMessageSenderServiceImpl msgMessageSenderService;

	@RequestMapping("/messagesender/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<MsgMessageSenderDto> msgMessageSenderDtos = msgMessageSenderService.list(contextDto);
		contextDto.setData(msgMessageSenderDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/messagesender/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		MsgMessageSenderDto msgMessageSenderDtos = msgMessageSenderService.load(contextDto);
		contextDto.setData(msgMessageSenderDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/messagesender/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		MsgMessageSenderDto msgMessageSenderDtos = msgMessageSenderService.save(contextDto);
		contextDto.setData(msgMessageSenderDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/messagesender/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = msgMessageSenderService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
