package com.labofjet.message.controller;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.message.service.MsgMessageSenderInfoService;
import com.labofjet.system.controller.CrudBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MsgMessageSenderInfoController extends CrudBaseController {

    @Autowired
    private MsgMessageSenderInfoService msgMessageSenderInfoService;

    @RequestMapping("/messagesenderinfo/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
        ContextDto result = msgMessageSenderInfoService.list(contextDto);
        return result;
    }
}
