package com.labofjet.message.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.controller.BaseController;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.message.dto.MsgShortMessageTemplateDto;
import com.labofjet.message.service.impl.MsgShortMessageTemplateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 * 手机短信模板  controller类
 */
@RestController
public class MsgShortMessageTemplateController extends BaseController {

    @Autowired
    private MsgShortMessageTemplateServiceImpl msgShortMessageTemplateService;

    @RequestMapping("/shortmessagetemplate/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
        PageInfo<MsgShortMessageTemplateDto> msgShortMessageTemplateDtos = msgShortMessageTemplateService.list(contextDto);
        msgShortMessageTemplateService.assembleUserForShortMessageTemplate(msgShortMessageTemplateDtos.getList());
        contextDto.setData(msgShortMessageTemplateDtos);
        contextDto.setSuccess(true);
        return contextDto;
    }

    @RequestMapping("/shortmessagetemplate/load")
    public ContextDto load(@RequestBody ContextDto contextDto) {
        MsgShortMessageTemplateDto msgShortMessageTemplateDtos = msgShortMessageTemplateService.load(contextDto);
        msgShortMessageTemplateService.assembleUserForShortMessageTemplate(Collections.singletonList(msgShortMessageTemplateDtos));
        contextDto.setData(msgShortMessageTemplateDtos);
        contextDto.setSuccess(true);
        return contextDto;
    }

    @RequestMapping("/shortmessagetemplate/save")
    public ContextDto save(@RequestBody ContextDto contextDto) {
        MsgShortMessageTemplateDto msgShortMessageTemplateDtos = msgShortMessageTemplateService.save(contextDto);
        contextDto.setData(msgShortMessageTemplateDtos);
        contextDto.setSuccess(true);
        return contextDto;
    }

    @RequestMapping("/shortmessagetemplate/delete")
    public ContextDto delete(@RequestBody ContextDto contextDto) {
        int i = msgShortMessageTemplateService.delete(contextDto);
        contextDto.setSuccess(true);
        contextDto.setMessage("成功删除了" + i + "条数据");
        return contextDto;
    }
}
