package com.labofjet.message.mapper;

import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.message.dto.MsgMessageSenderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MsgMessageSenderMapper {
	int countByExample(MsgMessageSenderExample example);

	int deleteByExample(MsgMessageSenderExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(MsgMessageSenderDto record);

	int insertSelective(MsgMessageSenderDto record);

	List<MsgMessageSenderDto> selectByExample(MsgMessageSenderExample example);

	MsgMessageSenderDto selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") MsgMessageSenderDto record, @Param("example") MsgMessageSenderExample example);

	int updateByExample(@Param("record") MsgMessageSenderDto record, @Param("example") MsgMessageSenderExample example);

	int updateByPrimaryKeySelective(MsgMessageSenderDto record);

	int updateByPrimaryKey(MsgMessageSenderDto record);

	//custom
	List<MsgMessageSenderDto> selectList(Map map);
}