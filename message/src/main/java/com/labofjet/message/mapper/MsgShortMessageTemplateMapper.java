package com.labofjet.message.mapper;

import com.labofjet.message.dto.MsgShortMessageTemplateDto;
import com.labofjet.message.dto.MsgShortMessageTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MsgShortMessageTemplateMapper {
    int countByExample(MsgShortMessageTemplateExample example);

    int deleteByExample(MsgShortMessageTemplateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MsgShortMessageTemplateDto record);

    int insertSelective(MsgShortMessageTemplateDto record);

    List<MsgShortMessageTemplateDto> selectByExample(MsgShortMessageTemplateExample example);

    MsgShortMessageTemplateDto selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MsgShortMessageTemplateDto record, @Param("example") MsgShortMessageTemplateExample example);

    int updateByExample(@Param("record") MsgShortMessageTemplateDto record, @Param("example") MsgShortMessageTemplateExample example);

    int updateByPrimaryKeySelective(MsgShortMessageTemplateDto record);

    int updateByPrimaryKey(MsgShortMessageTemplateDto record);

    //custom
    List<MsgShortMessageTemplateDto> selectList(Map map);
}