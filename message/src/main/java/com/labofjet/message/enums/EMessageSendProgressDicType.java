package com.labofjet.message.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EMessageSendProgressDicType implements BaseEnum {
	/**
	 * 提交发送请求
	 */
	SUBMIT("1", "提交发送请求"),
	/**
	 * 处理发送请求中
	 **/
	HADNLE("2", "处理发送请求中"),
	/**
	 * 处理发送请求完毕
	 **/
	FINISH("3", "处理发送请求完毕"),
	/**
	 * 处理发送请求完毕
	 **/
	ERROR("-1", "发生错误");

	private String code;
	private String text;

	EMessageSendProgressDicType(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}


