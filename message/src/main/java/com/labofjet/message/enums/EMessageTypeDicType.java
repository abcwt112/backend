package com.labofjet.message.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EMessageTypeDicType implements BaseEnum {
	/**
	 * 站内信
	 */
	SITE_MAIL("1", "站内信"),
	/**
	 * 邮件
	 **/
	EMAIL("2", "邮件"),
	/**
	 * 短信
	 **/
	MESSAGE("3", "短信");

	private String code;
	private String text;

	EMessageTypeDicType(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}


