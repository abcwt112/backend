package com.labofjet.message.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EMessageReceiverDicType implements BaseEnum {
	/**
	 * 按角色
	 */
	ROLE("1", "按角色"),
	/**
	 * 按用户
	 **/
	USER("2", "按用户");

	private String code;
	private String text;

	EMessageReceiverDicType(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}


