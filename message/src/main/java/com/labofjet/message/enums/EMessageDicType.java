package com.labofjet.message.enums;

import com.labofjet.common.enums.BaseEnum;

public enum EMessageDicType implements BaseEnum {
	/**
	 * 发送进度: 提交发送请求 处理发送请求中 处理发送请求完毕
	 */
	MESSAGE_SEND_PROGRESS("messageSendProgress", "messageSendProgress"),
	/**
	 * 接受者类型: 按角色 按用户
	 */
	MESSAGE_RECEIVER_TYPE("messageReceiverType", "messageReceiverType"),
	/**
	 * 消息类型: 站内信 邮件 短信
	 */
	MESSAGE_TYPE("messageType", "messageType");

	private String code;
	private String text;

	EMessageDicType(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
