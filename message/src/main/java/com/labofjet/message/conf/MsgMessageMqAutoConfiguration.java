package com.labofjet.message.conf;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MsgMessageMqProperties.class)
public class MsgMessageMqAutoConfiguration {
}

