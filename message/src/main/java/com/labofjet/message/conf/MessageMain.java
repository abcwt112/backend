package com.labofjet.message.conf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * 启动类
 */
@SpringBootApplication
@Import(com.labofjet.system.conf.SystemMain.class)
public class MessageMain {

	public static void main(String[] args) {
		SpringApplication.run(MessageMain.class, args);
	}

}