package com.labofjet.message.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MsgMessageSenderExample {
	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	public MsgMessageSenderExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andSnIsNull() {
			addCriterion("sn is null");
			return (Criteria) this;
		}

		public Criteria andSnIsNotNull() {
			addCriterion("sn is not null");
			return (Criteria) this;
		}

		public Criteria andSnEqualTo(String value) {
			addCriterion("sn =", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnNotEqualTo(String value) {
			addCriterion("sn <>", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnGreaterThan(String value) {
			addCriterion("sn >", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnGreaterThanOrEqualTo(String value) {
			addCriterion("sn >=", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnLessThan(String value) {
			addCriterion("sn <", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnLessThanOrEqualTo(String value) {
			addCriterion("sn <=", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnLike(String value) {
			addCriterion("sn like", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnNotLike(String value) {
			addCriterion("sn not like", value, "sn");
			return (Criteria) this;
		}

		public Criteria andSnIn(List<String> values) {
			addCriterion("sn in", values, "sn");
			return (Criteria) this;
		}

		public Criteria andSnNotIn(List<String> values) {
			addCriterion("sn not in", values, "sn");
			return (Criteria) this;
		}

		public Criteria andSnBetween(String value1, String value2) {
			addCriterion("sn between", value1, value2, "sn");
			return (Criteria) this;
		}

		public Criteria andSnNotBetween(String value1, String value2) {
			addCriterion("sn not between", value1, value2, "sn");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(String value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(String value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(String value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(String value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(String value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(String value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLike(String value) {
			addCriterion("type like", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotLike(String value) {
			addCriterion("type not like", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<String> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<String> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(String value1, String value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(String value1, String value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andProgressIsNull() {
			addCriterion("progress is null");
			return (Criteria) this;
		}

		public Criteria andProgressIsNotNull() {
			addCriterion("progress is not null");
			return (Criteria) this;
		}

		public Criteria andProgressEqualTo(String value) {
			addCriterion("progress =", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressNotEqualTo(String value) {
			addCriterion("progress <>", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressGreaterThan(String value) {
			addCriterion("progress >", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressGreaterThanOrEqualTo(String value) {
			addCriterion("progress >=", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressLessThan(String value) {
			addCriterion("progress <", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressLessThanOrEqualTo(String value) {
			addCriterion("progress <=", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressLike(String value) {
			addCriterion("progress like", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressNotLike(String value) {
			addCriterion("progress not like", value, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressIn(List<String> values) {
			addCriterion("progress in", values, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressNotIn(List<String> values) {
			addCriterion("progress not in", values, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressBetween(String value1, String value2) {
			addCriterion("progress between", value1, value2, "progress");
			return (Criteria) this;
		}

		public Criteria andProgressNotBetween(String value1, String value2) {
			addCriterion("progress not between", value1, value2, "progress");
			return (Criteria) this;
		}

		public Criteria andSenderIsNull() {
			addCriterion("sender is null");
			return (Criteria) this;
		}

		public Criteria andSenderIsNotNull() {
			addCriterion("sender is not null");
			return (Criteria) this;
		}

		public Criteria andSenderEqualTo(Integer value) {
			addCriterion("sender =", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotEqualTo(Integer value) {
			addCriterion("sender <>", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderGreaterThan(Integer value) {
			addCriterion("sender >", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderGreaterThanOrEqualTo(Integer value) {
			addCriterion("sender >=", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderLessThan(Integer value) {
			addCriterion("sender <", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderLessThanOrEqualTo(Integer value) {
			addCriterion("sender <=", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderIn(List<Integer> values) {
			addCriterion("sender in", values, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotIn(List<Integer> values) {
			addCriterion("sender not in", values, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderBetween(Integer value1, Integer value2) {
			addCriterion("sender between", value1, value2, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotBetween(Integer value1, Integer value2) {
			addCriterion("sender not between", value1, value2, "sender");
			return (Criteria) this;
		}

		public Criteria andReceiverIsNull() {
			addCriterion("receiver is null");
			return (Criteria) this;
		}

		public Criteria andReceiverIsNotNull() {
			addCriterion("receiver is not null");
			return (Criteria) this;
		}

		public Criteria andReceiverEqualTo(Integer value) {
			addCriterion("receiver =", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotEqualTo(Integer value) {
			addCriterion("receiver <>", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverGreaterThan(Integer value) {
			addCriterion("receiver >", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverGreaterThanOrEqualTo(Integer value) {
			addCriterion("receiver >=", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverLessThan(Integer value) {
			addCriterion("receiver <", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverLessThanOrEqualTo(Integer value) {
			addCriterion("receiver <=", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverIn(List<Integer> values) {
			addCriterion("receiver in", values, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotIn(List<Integer> values) {
			addCriterion("receiver not in", values, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverBetween(Integer value1, Integer value2) {
			addCriterion("receiver between", value1, value2, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotBetween(Integer value1, Integer value2) {
			addCriterion("receiver not between", value1, value2, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeIsNull() {
			addCriterion("receiver_type is null");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeIsNotNull() {
			addCriterion("receiver_type is not null");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeEqualTo(String value) {
			addCriterion("receiver_type =", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeNotEqualTo(String value) {
			addCriterion("receiver_type <>", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeGreaterThan(String value) {
			addCriterion("receiver_type >", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeGreaterThanOrEqualTo(String value) {
			addCriterion("receiver_type >=", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeLessThan(String value) {
			addCriterion("receiver_type <", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeLessThanOrEqualTo(String value) {
			addCriterion("receiver_type <=", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeLike(String value) {
			addCriterion("receiver_type like", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeNotLike(String value) {
			addCriterion("receiver_type not like", value, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeIn(List<String> values) {
			addCriterion("receiver_type in", values, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeNotIn(List<String> values) {
			addCriterion("receiver_type not in", values, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeBetween(String value1, String value2) {
			addCriterion("receiver_type between", value1, value2, "receiverType");
			return (Criteria) this;
		}

		public Criteria andReceiverTypeNotBetween(String value1, String value2) {
			addCriterion("receiver_type not between", value1, value2, "receiverType");
			return (Criteria) this;
		}

		public Criteria andTopicIsNull() {
			addCriterion("topic is null");
			return (Criteria) this;
		}

		public Criteria andTopicIsNotNull() {
			addCriterion("topic is not null");
			return (Criteria) this;
		}

		public Criteria andTopicEqualTo(String value) {
			addCriterion("topic =", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicNotEqualTo(String value) {
			addCriterion("topic <>", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicGreaterThan(String value) {
			addCriterion("topic >", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicGreaterThanOrEqualTo(String value) {
			addCriterion("topic >=", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicLessThan(String value) {
			addCriterion("topic <", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicLessThanOrEqualTo(String value) {
			addCriterion("topic <=", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicLike(String value) {
			addCriterion("topic like", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicNotLike(String value) {
			addCriterion("topic not like", value, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicIn(List<String> values) {
			addCriterion("topic in", values, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicNotIn(List<String> values) {
			addCriterion("topic not in", values, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicBetween(String value1, String value2) {
			addCriterion("topic between", value1, value2, "topic");
			return (Criteria) this;
		}

		public Criteria andTopicNotBetween(String value1, String value2) {
			addCriterion("topic not between", value1, value2, "topic");
			return (Criteria) this;
		}

		public Criteria andMessageIsNull() {
			addCriterion("message is null");
			return (Criteria) this;
		}

		public Criteria andMessageIsNotNull() {
			addCriterion("message is not null");
			return (Criteria) this;
		}

		public Criteria andMessageEqualTo(String value) {
			addCriterion("message =", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotEqualTo(String value) {
			addCriterion("message <>", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageGreaterThan(String value) {
			addCriterion("message >", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageGreaterThanOrEqualTo(String value) {
			addCriterion("message >=", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLessThan(String value) {
			addCriterion("message <", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLessThanOrEqualTo(String value) {
			addCriterion("message <=", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageLike(String value) {
			addCriterion("message like", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotLike(String value) {
			addCriterion("message not like", value, "message");
			return (Criteria) this;
		}

		public Criteria andMessageIn(List<String> values) {
			addCriterion("message in", values, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotIn(List<String> values) {
			addCriterion("message not in", values, "message");
			return (Criteria) this;
		}

		public Criteria andMessageBetween(String value1, String value2) {
			addCriterion("message between", value1, value2, "message");
			return (Criteria) this;
		}

		public Criteria andMessageNotBetween(String value1, String value2) {
			addCriterion("message not between", value1, value2, "message");
			return (Criteria) this;
		}

		public Criteria andAddTimeIsNull() {
			addCriterion("add_time is null");
			return (Criteria) this;
		}

		public Criteria andAddTimeIsNotNull() {
			addCriterion("add_time is not null");
			return (Criteria) this;
		}

		public Criteria andAddTimeEqualTo(Date value) {
			addCriterion("add_time =", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeNotEqualTo(Date value) {
			addCriterion("add_time <>", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeGreaterThan(Date value) {
			addCriterion("add_time >", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
			addCriterion("add_time >=", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeLessThan(Date value) {
			addCriterion("add_time <", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeLessThanOrEqualTo(Date value) {
			addCriterion("add_time <=", value, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeIn(List<Date> values) {
			addCriterion("add_time in", values, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeNotIn(List<Date> values) {
			addCriterion("add_time not in", values, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeBetween(Date value1, Date value2) {
			addCriterion("add_time between", value1, value2, "addTime");
			return (Criteria) this;
		}

		public Criteria andAddTimeNotBetween(Date value1, Date value2) {
			addCriterion("add_time not between", value1, value2, "addTime");
			return (Criteria) this;
		}
	}

	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}