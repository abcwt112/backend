package com.labofjet.message.dto;

public class MsgShortMessageTemplateDto {
    private Integer id;

    private String signName;

    private String templateCode;

    private String templateParam;

    private String smsUpExtendCode;

    private String outId;

    private Integer userId;

    private String addUserName;

    private String text;

    private String realTemplateParam; // 解析完成的params

    public String getRealTemplateParam() {
        return realTemplateParam;
    }

    public void setRealTemplateParam(String realTemplateParam) {
        this.realTemplateParam = realTemplateParam;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAddUserName() {
        return addUserName;
    }

    public void setAddUserName(String addUserName) {
        this.addUserName = addUserName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateParam() {
        return templateParam;
    }

    public void setTemplateParam(String templateParam) {
        this.templateParam = templateParam;
    }

    public String getSmsUpExtendCode() {
        return smsUpExtendCode;
    }

    public void setSmsUpExtendCode(String smsUpExtendCode) {
        this.smsUpExtendCode = smsUpExtendCode;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "MsgShortMessageTemplateDto{" +
                "id=" + id +
                ", signName='" + signName + '\'' +
                ", templateCode='" + templateCode + '\'' +
                ", templateParam='" + templateParam + '\'' +
                ", smsUpExtendCode='" + smsUpExtendCode + '\'' +
                ", outId='" + outId + '\'' +
                ", userId=" + userId +
                ", addUserName='" + addUserName + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}