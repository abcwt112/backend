package com.labofjet.message.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class MsgMessageSiteMailDto {
	private String id;

	private String sn;

	private String type;

	private String typeName;

	private String progress;

	private String progressName;

	private Integer sender;

	private String receiver;

	private String receiverType;

	private String receiverTypeName;

	private String topic;

	private String message;

	private String senderName;

	private String receiverUsername;

	private Boolean read;

	private Integer receiverId;

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;

	public String getReceiverUsername() {
		return receiverUsername;
	}

	public void setReceiverUsername(String receiverUsername) {
		this.receiverUsername = receiverUsername;
	}

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public String getProgressName() {
		return progressName;
	}

	public void setProgressName(String progressName) {
		this.progressName = progressName;
	}

	public Integer getSender() {
		return sender;
	}

	public void setSender(Integer sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public String getReceiverTypeName() {
		return receiverTypeName;
	}

	public void setReceiverTypeName(String receiverTypeName) {
		this.receiverTypeName = receiverTypeName;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	@Override
	public String toString() {
		return "MsgMessageSiteMailDto{" +
				"id='" + id + '\'' +
				", sn='" + sn + '\'' +
				", type='" + type + '\'' +
				", typeName='" + typeName + '\'' +
				", progress='" + progress + '\'' +
				", progressName='" + progressName + '\'' +
				", sender=" + sender +
				", receiver='" + receiver + '\'' +
				", receiverType='" + receiverType + '\'' +
				", receiverTypeName='" + receiverTypeName + '\'' +
				", topic='" + topic + '\'' +
				", message='" + message + '\'' +
				", senderName='" + senderName + '\'' +
				", receiverUsername='" + receiverUsername + '\'' +
				", read=" + read +
				", receiverId=" + receiverId +
				", addTime=" + addTime +
				'}';
	}
}