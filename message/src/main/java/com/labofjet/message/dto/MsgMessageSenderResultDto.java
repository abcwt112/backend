package com.labofjet.message.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 消息发送结果
 */
public class MsgMessageSenderResultDto {
	private String id;

	private String result; // 发送结果
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date; // 发送日期

	private String username;

	private String sn; // 编号

	private String failMessage;

	public String getFailMessage() {
		return failMessage;
	}

	public void setFailMessage(String failMessage) {
		this.failMessage = failMessage;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "MsgMessageSenderResultDto{" +
				"id='" + id + '\'' +
				", result='" + result + '\'' +
				", date=" + date +
				", username='" + username + '\'' +
				", sn='" + sn + '\'' +
				'}';
	}
}