package com.labofjet.message.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.service.MqService;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.message.conf.MsgMessageMqProperties;
import com.labofjet.message.dto.MsgMessageSenderDto;
import com.labofjet.message.dto.MsgMessageSenderExample;
import com.labofjet.message.enums.EMessageDicType;
import com.labofjet.message.enums.EMessageSendProgressDicType;
import com.labofjet.message.mapper.MsgMessageSenderMapper;
import com.labofjet.message.util.MessageIdUtils;
import com.labofjet.system.service.impl.CrudBaseServiceImpl;
import com.labofjet.system.util.SysCommonUtils;
import com.labofjet.system.util.SysDicUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 消息  serviceImpl类
 */
@Service
public class MsgMessageSenderServiceImpl extends CrudBaseServiceImpl<MsgMessageSenderDto> {
	@Resource
	private MsgMessageSenderMapper msgMessageSenderMapper;

	@Autowired
	private MqService mqService;

	@Autowired
	private MsgMessageMqProperties msgMessageMqProperties;

	@Autowired
	private MessageIdUtils messageIdUtils;

	@Override
	public PageInfo<MsgMessageSenderDto> list(ContextDto result) {
		Map<String, Object> map = super.prepare(result);
		List<MsgMessageSenderDto> MsgMessageSenderDtos = msgMessageSenderMapper.selectList(map);
		// 设置字典值
		SysDicUtils.combineDicInfo(MsgMessageSenderDtos, new SysDicUtils.DicInfoHandler<MsgMessageSenderDto>() {
			@Override
			public void handle(MsgMessageSenderDto s) {
				s.setReceiverTypeName(SysDicUtils.getDicInfoTextBySn(EMessageDicType.MESSAGE_RECEIVER_TYPE, s.getReceiverType()));
				s.setTypeName(SysDicUtils.getDicInfoTextBySn(EMessageDicType.MESSAGE_TYPE, s.getType()));
				s.setProgressName(SysDicUtils.getDicInfoTextBySn(EMessageDicType.MESSAGE_SEND_PROGRESS, s.getProgress()));
			}
		});
		return new PageInfo<>(MsgMessageSenderDtos);
	}

	@Override
	public MsgMessageSenderDto load(ContextDto result) {
		Map<String, Object> map = super.getRequestParamsMap(result);
		Integer id = (Integer) map.get("id");
		if (null != id) {
			return msgMessageSenderMapper.selectList(map).get(0);
		}
		return null;
	}

	@Override
	@Transactional
	public MsgMessageSenderDto save(ContextDto result) {
		MsgMessageSenderDto dto = JSONUtils.getRequestData(result, MsgMessageSenderDto.class);
		if (dto.getId() != null) {
			msgMessageSenderMapper.updateByPrimaryKey(dto);
		} else {
			dto.setSn(messageIdUtils.generateMessageSenderSn());
			dto.setAddTime(new Date());
			dto.setSender(SysCommonUtils.getCurrentUser().getId());
			dto.setSenderName(SysCommonUtils.getCurrentUser().getUsername());
			dto.setProgress(EMessageSendProgressDicType.SUBMIT.getCode());
			msgMessageSenderMapper.insertSelective(dto);
			mqService.sendMessage(msgMessageMqProperties.getExchangeName(), msgMessageMqProperties.getRoutingKey(), JSONUtils.dtoToString(dto));
		}
		return dto;
	}

	@Override
	@Transactional
	public int delete(ContextDto result) {
		List<Integer> ids = JSONUtils.getRequestData(result, List.class);
		MsgMessageSenderExample example = new MsgMessageSenderExample();
		example.createCriteria().andIdIn(ids);
		int i = msgMessageSenderMapper.deleteByExample(example);
		return i;
	}
}
