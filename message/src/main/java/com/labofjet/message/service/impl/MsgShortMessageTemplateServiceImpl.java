package com.labofjet.message.service.impl;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.common.util.ListHelpers;
import com.labofjet.message.dto.MsgShortMessageTemplateDto;
import com.labofjet.message.dto.MsgShortMessageTemplateExample;
import com.labofjet.message.mapper.MsgShortMessageTemplateMapper;
import com.labofjet.system.dto.SysUserDto;
import com.labofjet.system.dto.SysUserExample;
import com.labofjet.system.service.impl.CrudBaseServiceImpl;
import com.labofjet.system.service.impl.SysUserServiceImpl;
import com.labofjet.system.util.SysCommonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 手机短信模板  serviceImpl类
 */
@Service
public class MsgShortMessageTemplateServiceImpl extends CrudBaseServiceImpl<MsgShortMessageTemplateDto> {
    @Resource
    private MsgShortMessageTemplateMapper msgShortMessageTemplateMapper;

    @Autowired
    private SysUserServiceImpl sysUserService;

    public List<MsgShortMessageTemplateDto> listPage(ContextDto result) {
        Map<String, Object> map = super.prepare(result);
        List<MsgShortMessageTemplateDto> msgShortMessageTemplateDtos = msgShortMessageTemplateMapper.selectList(map);
        return msgShortMessageTemplateDtos;
    }

    @Override
    public PageInfo<MsgShortMessageTemplateDto> list(ContextDto result) {
        Map<String, Object> map = super.prepare(result);
        Integer userId = SysCommonUtils.getCurrentUser().getId();
        if(map.get("userId") == null && userId != 1) {
            map.put("userId", userId);
        }
        List<MsgShortMessageTemplateDto> MsgShortMessageTemplateDtos = msgShortMessageTemplateMapper.selectList(map);
        return new PageInfo<>(MsgShortMessageTemplateDtos);
    }

    @Override
    public MsgShortMessageTemplateDto load(ContextDto result) {
        Map<String, Object> map = super.getRequestParamsMap(result);
        Integer id = (Integer) map.get("id");
        if (null != id) {
            return msgShortMessageTemplateMapper.selectList(map).get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public MsgShortMessageTemplateDto save(ContextDto result) {
        MsgShortMessageTemplateDto dto = JSONUtils.getRequestData(result, MsgShortMessageTemplateDto.class);
        if (dto.getId() != null) {
            msgShortMessageTemplateMapper.updateByPrimaryKey(dto);
        } else {
            dto.setUserId(SysCommonUtils.getCurrentUser().getId()); // 添加人
            msgShortMessageTemplateMapper.insertSelective(dto);
        }
        return dto;
    }

    @Override
    @Transactional
    public int delete(ContextDto result) {
        List<Integer> ids = JSONUtils.getRequestData(result, List.class);
        MsgShortMessageTemplateExample example = new MsgShortMessageTemplateExample();
        example.createCriteria().andIdIn(ids);
        int i = msgShortMessageTemplateMapper.deleteByExample(example);
        return i;
    }


    /**
     * 给短信发送模板设置关联的用户
     *
     * @param msgShortMessageTemplateDtos
     */
    public void assembleUserForShortMessageTemplate(List<MsgShortMessageTemplateDto> msgShortMessageTemplateDtos) {
        if (CollectionUtils.isEmpty(msgShortMessageTemplateDtos)) { // 如果参数为空
            return;
        }

        List<Integer> userIds = new ArrayList<>();
        for (MsgShortMessageTemplateDto msgShortMessageTemplateDto : msgShortMessageTemplateDtos) { // 取出所有用户ID一次查找对应的角色
            userIds.add(msgShortMessageTemplateDto.getUserId());
        }

        SysUserExample sysUserExample = new SysUserExample();
        sysUserExample.createCriteria().andIdIn(userIds);
        List<SysUserDto> sysUserDtos = sysUserService.selectByExample(sysUserExample);
        if (CollectionUtils.isEmpty(sysUserDtos)) {
            return;
        }
        ListHelpers.combineList(msgShortMessageTemplateDtos, sysUserDtos, new ListHelpers.Match<MsgShortMessageTemplateDto, SysUserDto>() {

            @Override
            public void whenMatch(MsgShortMessageTemplateDto msgShortMessageTemplateDto, SysUserDto sysUserDto) {
                msgShortMessageTemplateDto.setAddUserName(sysUserDto.getUsername());
            }

            @Override
            public boolean match(MsgShortMessageTemplateDto msgShortMessageTemplateDto, SysUserDto sysUserDto) {
                return msgShortMessageTemplateDto.getUserId().equals(sysUserDto.getId());
            }
        });
    }

    public List<MsgShortMessageTemplateDto> selectByExample(MsgShortMessageTemplateExample msgShortMessageTemplateExample) {
        return msgShortMessageTemplateMapper.selectByExample(msgShortMessageTemplateExample);
    }
}
