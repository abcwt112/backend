package com.labofjet.message.service;

import com.labofjet.common.dto.ContextDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 站内信  service类
 */
@FeignClient(value = "message-sender")
public interface MsgMessageSiteMailService {
	/**
	 * 列表查询
	 *
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/messagesitemail/list", method = RequestMethod.POST)
	ContextDto list(ContextDto result);

	/**
	 * 加载单条
	 *
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/messagesitemail/load", method = RequestMethod.POST)
	ContextDto load(ContextDto result);

	/**
	 * 统计数量
	 *
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/messagesitemail/count", method = RequestMethod.POST)
	ContextDto count(ContextDto result);

	/**
	 * 设置已读或者未读
	 *
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/messagesitemail/readUnRead", method = RequestMethod.POST)
	ContextDto readUnRead(ContextDto result);
}
