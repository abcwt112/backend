package com.labofjet.message.service;

import com.labofjet.common.dto.ContextDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "message-sender")
public interface MsgMessageSenderInfoService {


    @RequestMapping(value = "/messagesenderinfo/list" , method = RequestMethod.POST)
    ContextDto list(ContextDto contextDto);
}
