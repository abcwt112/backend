package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysMenuDto;
import com.labofjet.system.service.impl.SysMenuServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 菜单
 */
@RestController
public class SysMenuController extends CrudBaseController {

	@Autowired
	private SysMenuServiceImpl sysMenuService;

	/**
	 * 页菜单查询,要过滤权限
	 *
	 * @param contextDto
	 * @return
	 */
	@RequestMapping("/menu/menuList")
	public ContextDto menuList(@RequestBody ContextDto contextDto) {
		PageInfo<SysMenuDto> sysMenuDtos = sysMenuService.menuList(contextDto);
		contextDto.setData(sysMenuDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/menu/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysMenuDto> sysMenuDtos = sysMenuService.list(contextDto);
		contextDto.setData(sysMenuDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/menu/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysMenuDto sysMenuDtos = sysMenuService.load(contextDto);
		contextDto.setData(sysMenuDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/menu/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysMenuDto sysMenuDtos = sysMenuService.save(contextDto);
		contextDto.setData(sysMenuDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/menu/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysMenuService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
