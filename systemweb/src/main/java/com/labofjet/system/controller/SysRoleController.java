package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysRoleDto;
import com.labofjet.system.service.impl.SysPermissionServiceImpl;
import com.labofjet.system.service.impl.SysRoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Role页面信息
 */
@RestController
public class SysRoleController extends CrudBaseController {

	@Autowired
	private SysRoleServiceImpl sysRoleService;

	@Autowired
	private SysPermissionServiceImpl sysPermissionService;


	@RequestMapping("/role/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysRoleDto> SysRoleDtos = sysRoleService.list(contextDto);
		contextDto.setData(SysRoleDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/role/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysRoleDto sysRoleDto = sysRoleService.load(contextDto);
		if(sysRoleDto != null) {
			Map<String, Object> params = new HashMap<>();
			params.put("roleId", sysRoleDto.getId());
			ContextDto context = new ContextDto();
			context.setRequestParams(params);
			sysRoleDto.setSysPermissionDtoList(sysPermissionService.list(context).getList()); // 加载关联的permission
		}
		contextDto.setData(sysRoleDto);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/role/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysRoleDto SysRoleDtos = sysRoleService.save(contextDto);
		contextDto.setData(SysRoleDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/role/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysRoleService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
