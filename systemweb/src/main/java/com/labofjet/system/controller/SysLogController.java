package com.labofjet.system.controller;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.log.annotation.NoAopLog;
import com.labofjet.system.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 日志查询
 */
@NoAopLog
@RestController
public class SysLogController extends CrudBaseController {

	@Autowired
	private SysLogService sysLogService;

	@RequestMapping("/log/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		log.info("调用 SysLogController.list 参数为{} ", contextDto);
		Map<String, Object> requestParams = contextDto.getRequestParams();
		ContextDto result = sysLogService.list(contextDto);

		return result;
	}

	@RequestMapping("/log/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		log.info("调用 SysLogController.load 参数为{} ", contextDto);
		ContextDto result = sysLogService.load(contextDto);

		return result;
	}

//	@RequestMapping("/menu/save")
//	public ContextDto save(@RequestBody ContextDto contextDto) {
//		LogDto sysMenuDtos = sysMenuService.save(contextDto);
//		contextDto.setData(sysMenuDtos);
//		contextDto.setSuccess(true);
//
//		return contextDto;
//	}
//
//	@RequestMapping("/menu/delete")
//	public ContextDto delete(@RequestBody ContextDto contextDto) {
//		int i = sysMenuService.delete(contextDto);
//		contextDto.setSuccess(true);
//		contextDto.setMessage("成功删除了" + i + "条数据");
//		return contextDto;
//	}
}
