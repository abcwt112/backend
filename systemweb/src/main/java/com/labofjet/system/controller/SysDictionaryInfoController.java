package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysDictionaryInfoDto;
import com.labofjet.system.service.impl.SysDictionaryInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 字典详情  controller类
 */
@RestController
public class SysDictionaryInfoController extends CrudBaseController {

	@Autowired
	private SysDictionaryInfoServiceImpl sysDictionaryInfoService;

	@RequestMapping("/dictionaryinfo/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysDictionaryInfoDto> sysDictionaryInfoDtos = sysDictionaryInfoService.list(contextDto);
		contextDto.setData(sysDictionaryInfoDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionaryinfo/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysDictionaryInfoDto sysDictionaryInfoDtos = sysDictionaryInfoService.load(contextDto);
		contextDto.setData(sysDictionaryInfoDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionaryinfo/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysDictionaryInfoDto sysDictionaryInfoDtos = sysDictionaryInfoService.save(contextDto);
		contextDto.setData(sysDictionaryInfoDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionaryinfo/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysDictionaryInfoService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
