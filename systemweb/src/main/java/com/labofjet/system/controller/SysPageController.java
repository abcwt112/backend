package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysPageDto;
import com.labofjet.system.service.impl.SysPageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * common页面信息
 */
@RestController
public class SysPageController extends CrudBaseController {

	@Autowired
	private SysPageServiceImpl sysPageService;

	@RequestMapping("/page/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysPageDto> sysPageDtos = sysPageService.list(contextDto);
		contextDto.setData(sysPageDtos);
		contextDto.setSuccess(true);
		log.info("info日志: 执行方法 SysPageController.list ");
		return contextDto;
	}

	@RequestMapping("/page/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysPageDto sysPageDtos = sysPageService.load(contextDto);
		contextDto.setData(sysPageDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/page/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysPageDto sysPageDtos = sysPageService.save(contextDto);
		contextDto.setData(sysPageDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/page/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysPageService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
