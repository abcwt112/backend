package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysPermissionDto;
import com.labofjet.system.service.impl.SysPermissionServiceImpl;
import com.labofjet.system.service.impl.SysRoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Permission页面信息
 */
@RestController
public class SysPermissionController extends CrudBaseController {

	@Autowired
	private SysPermissionServiceImpl sysPermissionService;

	@Autowired
	private SysRoleServiceImpl sysRoleService;


	@RequestMapping("/permission/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysPermissionDto> SysPermissionDtos = sysPermissionService.list(contextDto);
		contextDto.setData(SysPermissionDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/permission/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysPermissionDto SysPermissionDtos = sysPermissionService.load(contextDto);
		if(SysPermissionDtos != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("permissionId", SysPermissionDtos.getId());
			ContextDto context = new ContextDto();
			context.setRequestParams(map);
			SysPermissionDtos.setSysRoleDtoList(sysRoleService.list(context).getList()); // 加载关联的role
		}
		contextDto.setData(SysPermissionDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/permission/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysPermissionDto SysPermissionDtos = sysPermissionService.save(contextDto);
		contextDto.setData(SysPermissionDtos);
		contextDto.setSuccess(true);

		return contextDto;
	}

	@RequestMapping("/permission/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysPermissionService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
