package com.labofjet.system.controller;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysCodeDto;
import com.labofjet.system.service.impl.SysCodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 代码生成
 */
@RestController
public class SysCodeController extends CrudBaseController {

	@Autowired
	private SysCodeServiceImpl codeService;

	@RequestMapping("/code/generate")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		SysCodeDto sysCodeDto = codeService.generateCode(contextDto);
		contextDto.setSuccess(true);
		contextDto.setData(sysCodeDto);
		return contextDto;
	}

}
