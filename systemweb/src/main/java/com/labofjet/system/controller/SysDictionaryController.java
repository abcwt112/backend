package com.labofjet.system.controller;

import com.github.pagehelper.PageInfo;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.system.dto.SysDictionaryDto;
import com.labofjet.system.service.impl.SysDictionaryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller类
 */
@RestController
public class SysDictionaryController extends CrudBaseController {

	@Autowired
	private SysDictionaryServiceImpl sysDictionaryService;

	@RequestMapping("/dictionary/list")
	public ContextDto list(@RequestBody ContextDto contextDto) {
		PageInfo<SysDictionaryDto> sysDictionaryDtos = sysDictionaryService.list(contextDto);
		contextDto.setData(sysDictionaryDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionary/load")
	public ContextDto load(@RequestBody ContextDto contextDto) {
		SysDictionaryDto sysDictionaryDtos = sysDictionaryService.load(contextDto);
		contextDto.setData(sysDictionaryDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionary/save")
	public ContextDto save(@RequestBody ContextDto contextDto) {
		SysDictionaryDto sysDictionaryDtos = sysDictionaryService.save(contextDto);
		contextDto.setData(sysDictionaryDtos);
		contextDto.setSuccess(true);
		return contextDto;
	}

	@RequestMapping("/dictionary/delete")
	public ContextDto delete(@RequestBody ContextDto contextDto) {
		int i = sysDictionaryService.delete(contextDto);
		contextDto.setSuccess(true);
		contextDto.setMessage("成功删除了" + i + "条数据");
		return contextDto;
	}
}
