package com.labofjet.system.conf;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ConfigurationProperties(prefix = "system.whitelist")
public class WhiteListProperties implements InitializingBean {

	private String urls;

	private List<String> urlList;

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}

	public List<String> getUrlList() {
		return urlList;
	}

	public void setUrlList(List<String> urlList) {
		this.urlList = urlList;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (StringUtils.isNotBlank(urls) && CollectionUtils.isEmpty(urlList)) {
			urlList = new ArrayList<>();
			urlList.addAll(Arrays.asList(urls.split(",")));
		}
	}
}
