package com.labofjet.system.conf;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(WhiteListProperties.class)
public class WhiteListAutoConfiguration {


}
