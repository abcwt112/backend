//package com.labofjet.spring.boot.admin.server.controller;
//
//import com.labofjet.common.dto.ContextDto;
//import com.labofjet.common.dto.LogDto;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class LoginController {
//
//	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
//
//	@RequestMapping("/admin/login")
//	public ContextDto login(@RequestBody ContextDto contextDto) {
//		LOG.info("调用 LoginController.login 参数为{} ", contextDto);
//		contextDto.setData(contextDto);
//		contextDto.setSuccess(true);
//		return contextDto;
//	}
//}
