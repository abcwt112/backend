package com.labofjet.common.util;


import org.apache.commons.lang3.StringUtils;

public class StringHelper {
    public static String toString(Object o) {
        return o == null ? null : o.toString();
    }

    public static String defaultIfEmpty(Object o, String defaultStr) {
        return StringUtils.defaultIfEmpty(toString(o), defaultStr);
    }
}
