package com.labofjet.common.util;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * list辅助操作类
 */
public class ListHelpers {

	/**
	 * 常用于分页查询了1个list a, 有查询了1个list b 设置a的成员域b.
	 *
	 * @param src
	 * @param tgt
	 * @param mc
	 * @param <S>
	 * @param <T>
	 */
	public static <S, T> void combineList(List<S> src, List<T> tgt, Match<S, T> mc) {
		// 当list中有1方为空
		if (CollectionUtils.isEmpty(src) || CollectionUtils.isEmpty(tgt)) {
			return;
		}
		for (S s : src) {
			if (!mc.beforeMatch(s)) {
				continue;
			}
			int hits = 0;
			for (T t : tgt) {
				if (mc.match(s, t)) {
					hits++;
					mc.whenMatch(s, t);
				}
			}
			mc.afterMatch(s, hits);
		}
	}

	public interface Match<S, T> {

		/**
		 * match之前,可以做一些初始化
		 *
		 * @param s
		 * @return false可以阻止match和afterMatch
		 */
		default boolean beforeMatch(S s) {
			return true;
		}

		/**
		 * 如何匹配2个list内的元素
		 */
		default boolean match(S s, T t) {
			return false;
		}

		/**
		 * 当2个list的元素匹配时做的事情
		 *
		 * @param s
		 * @param t
		 */
		default void whenMatch(S s, T t) {
		}

		/**
		 * match之后,基本没卵用
		 *
		 * @param s
		 */
		default void afterMatch(S s, int hits) {
		}
	}
}
