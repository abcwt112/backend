package com.labofjet.common.util;

/**
 * 自定义的转换
 *
 * @author jyzjyz12@163.com
 */
public interface CustomConverter {
	void convert(Object source, Object target);
}
