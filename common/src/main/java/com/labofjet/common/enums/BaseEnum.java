package com.labofjet.common.enums;

/**
 * 枚举需要实现的公共接口
 *
 * @author jyzjyz12@163.com
 */
public interface BaseEnum {
	public String getCode();

	public String getText();
}
