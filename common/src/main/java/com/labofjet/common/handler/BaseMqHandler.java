package com.labofjet.common.handler;


public interface BaseMqHandler {

	/**
	 * mq handler 的具体处理逻辑
	 *
	 * @param s
	 */
	public void handle(String s);
}
