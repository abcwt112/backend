package com.labofjet.common.handler.impl;

import com.labofjet.common.handler.BaseMqHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;

/**
 * mq的handler
 */
public abstract class BaseMqHandlerImpl implements BaseMqHandler {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@RabbitHandler
	public void doHandle(String s) {
		try {
			this.handle(s);
		} catch (Exception e) {
			log.error("ma handler 出现错误", e);
		}
	}

}

