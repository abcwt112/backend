package com.labofjet.common.dto;

import java.util.List;

public class PageDto<T> {
	private List<T> list;
	private Long total;

	public PageDto() {

	}

	public PageDto(List<T> list) {
		this.list = list;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "PageDto{" +
				"list=" + list +
				", total=" + total +
				'}';
	}
}
