package com.labofjet.common.dto;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

/**
 * 前后台通信基础DTO
 */
public class ContextDto {
	/**
	 * 请求是否成功
	 */
	private Boolean success;
	/**
	 * 前台提示消息
	 */
	private String message;
	/**
	 * 如果success=false的话是哪种类型的失败
	 */
	private String code;

	/**
	 * 请求参数
	 */
	private Map<String, Object> requestParams = new HashMap<>();

	private JsonNode requestData;

	private Object data;
	private Object data2;
	private Object data3;

	public static ContextDto valueOfSuccess() {
		ContextDto me = new ContextDto();
		me.setSuccess(true);
		return me;
	}

	public Map<String, Object> getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(Map<String, Object> requestParams) {
		this.requestParams = requestParams;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getData2() {
		return data2;
	}

	public void setData2(Object data2) {
		this.data2 = data2;
	}

	public Object getData3() {
		return data3;
	}

	public void setData3(Object data3) {
		this.data3 = data3;
	}

	public JsonNode getRequestData() {
		return requestData;
	}

	public void setRequestData(JsonNode requestData) {
		this.requestData = requestData;
	}

	@Override
	public String toString() {
		return "ContextDto{" +
				"success=" + success +
				", message='" + message + '\'' +
				", code='" + code + '\'' +
				", requestParams=" + requestParams +
				", data=" + data +
				", data2=" + data2 +
				", data3=" + data3 +
				'}';
	}
}
