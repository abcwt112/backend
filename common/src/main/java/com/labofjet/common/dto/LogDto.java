package com.labofjet.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 收集日志做的格式封装
 */
public class LogDto {
	private String id;
	private String ip; // 本机IP
	private String uuid; // 便于追踪信息
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private String message; // 日志
	private String level;
	private String date; // 发生时间
	private String threadName;

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "LogDto{" +
				"id='" + id + '\'' +
				", ip='" + ip + '\'' +
				", uuid='" + uuid + '\'' +
				", message='" + message + '\'' +
				", level='" + level + '\'' +
				", date=" + date +
				", threadName='" + threadName + '\'' +
				'}';
	}
}
