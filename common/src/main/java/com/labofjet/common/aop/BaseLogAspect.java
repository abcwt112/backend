package com.labofjet.common.aop;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * 基础日志记录
 *
 * @author jyzjyz12@163.com
 */
public abstract class BaseLogAspect {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	protected Object doLog(ProceedingJoinPoint pjp, String type) throws Throwable {// NOSONAR
		UUID uuid = UUID.randomUUID();
		String signature = pjp.getSignature().toString();
		logger.info("识别号[sequenceid:{}]: 调用{}方法: {}", uuid, type, signature);
		logger.debug("识别号[sequenceid:{}]: 参数为 {}", uuid, ArrayUtils.toString(pjp.getArgs()));
		try {
			long start = System.currentTimeMillis();
			Object result = pjp.proceed();
			logger.info("识别号[sequenceid:{}]: 调用{}方法: {} 成功! 返回 {}", uuid, type, signature, result);
			logger.info("识别号[sequenceid:{}]: 调用消耗时间为[timecost:{}]", uuid, System.currentTimeMillis() - start);
			return result;
		} catch (Exception e) {
			logger.error("识别号[sequenceid:{}]: 调用{}方法: {} 失败! 输入为{}", uuid, type, signature,
					ArrayUtils.toString(pjp.getArgs()));
			logger.error("识别号[sequenceid:{}]: 异常为: ", uuid, e);
			throw e;
		}
	}

}
