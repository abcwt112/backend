package com.labofjet.common.aop;

/**
 * 不同aspect的优先级
 *
 * @author jyzjyz12@163.com
 */
public interface AspectOrder {

	int LOG_ORDER = -10000;

	int URLPERMISSION_ORDER = -500;


	int CHECKEXCEPTION_ORDER = 500;

	int CACHE_ORDER = 1000;



}
