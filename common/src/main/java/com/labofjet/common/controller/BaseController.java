package com.labofjet.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础controller
 */
public class BaseController {
	protected Logger log = LoggerFactory.getLogger(getClass());
}

