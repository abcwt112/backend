package com.labofjet.common.log.appender;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.labofjet.common.aop.LogAspect;

public class RabbitMqLogAsyncAppender extends AsyncAppender {

	protected void preprocess(ILoggingEvent eventObject) {
		LogAspect.setLogDto(eventObject, LogAspect.getLogDto());
		super.preprocess(eventObject);
	}

}
