package com.labofjet.common.log.annotation;

import java.lang.annotation.*;

/**
 * 跳过日志,主要用于aop日志
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface NoAopLog {
}
