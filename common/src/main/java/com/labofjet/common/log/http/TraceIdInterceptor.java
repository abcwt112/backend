package com.labofjet.common.log.http;

import com.labofjet.common.log.enums.ELogDefine;
import com.labofjet.common.util.CommonUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * 为全局HTTP请求添加traceId用于全局日志追踪
 */
public class TraceIdInterceptor implements RequestInterceptor {
	@Override
	public void apply(RequestTemplate template) {
		template.header(ELogDefine.TRACEID.getCode(), CommonUtils.getTraceId());
	}
}
