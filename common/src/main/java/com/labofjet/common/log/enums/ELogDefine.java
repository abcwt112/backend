package com.labofjet.common.log.enums;

import com.labofjet.common.enums.BaseEnum;

public enum ELogDefine implements BaseEnum {
	TRACEID("traceId", "traceId");

	private String code;
	private String text;

	ELogDefine(String code, String text) {
		this.code = code;
		this.text = text;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
