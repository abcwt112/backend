package com.labofjet.common.autoconf.log;

import com.labofjet.common.log.http.TraceIdInterceptor;
import feign.Feign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnMissingBean(name = "traceIdInterceptor")
@ConditionalOnClass(Feign.class)
public class TraceIdConf {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public TraceIdConf() {
	}

	@Bean
	public TraceIdInterceptor traceIdInterceptor() {
		log.debug("TraceIdConf");
		return new TraceIdInterceptor();
	}
}
