package com.labofjet.common.autoconf.datsource;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 其他模块不配置的话就用这个当做默认的mongodb配置
 */
@Configuration
@ConditionalOnClass(MongoClient.class)
public class MongoConf {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Bean
	@ConditionalOnMissingBean(MongoClientOptions.class)
	public MongoClientOptions mongoClientOptions() {
		log.debug("初始化 MongoClientOptions");
		return MongoClientOptions.builder().minConnectionsPerHost(3).build();
	}
}

