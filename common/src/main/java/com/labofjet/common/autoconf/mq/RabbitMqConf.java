package com.labofjet.common.autoconf.mq;

import com.labofjet.common.service.MqService;
import com.labofjet.common.service.impl.RabbitMqServiceImpl;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置rabbitmq
 */
@Configuration
@ConditionalOnClass({RabbitTemplate.class, Channel.class})
public class RabbitMqConf {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public RabbitMqConf() {
	}

	@Bean
	public MqService mqService() {
		log.debug("初始化RabbitMqConf");
		return new RabbitMqServiceImpl();
	}
}
