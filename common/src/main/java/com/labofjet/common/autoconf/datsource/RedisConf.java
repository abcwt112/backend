package com.labofjet.common.autoconf.datsource;

import com.labofjet.common.service.RedisService;
import com.labofjet.common.service.impl.RedisServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@ConditionalOnClass(RedisTemplate.class)
public class RedisConf {

	@Bean("redisService")
	public RedisService redisService() {
		return new RedisServiceImpl();
	}
}
