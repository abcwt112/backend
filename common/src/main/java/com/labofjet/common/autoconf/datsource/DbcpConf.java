package com.labofjet.common.autoconf.datsource;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
@ConditionalOnClass(BasicDataSource.class)
public class DbcpConf {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public DbcpConf() {
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource datasource() {
		log.debug("初始化dbcp数据源");
		DataSource datasource = new BasicDataSource();
		return datasource;
	}
}
