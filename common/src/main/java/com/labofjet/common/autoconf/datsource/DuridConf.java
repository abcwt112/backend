package com.labofjet.common.autoconf.datsource;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 配置数据源
 */
@Configuration
@ConditionalOnClass(DruidDataSource.class)
public class DuridConf {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public DuridConf() {
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		log.debug("初始化durid数据源");
		DruidDataSource datasource = new DruidDataSource();
		return datasource;
	}
}
