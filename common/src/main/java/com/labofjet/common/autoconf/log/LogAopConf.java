package com.labofjet.common.autoconf.log;

import com.labofjet.common.aop.LogAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置logAop
 */
@Configuration
@ConditionalOnMissingBean(name = "logAspect")
public class LogAopConf {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public LogAopConf() {
	}

	@Bean
	public LogAspect logAspect() {
		log.debug("初始化LogAopConf");
		return new LogAspect();
	}
}
