package com.labofjet.common.service.impl;

import com.labofjet.common.service.MqService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;


public class RabbitMqServiceImpl extends BaseServiceImpl implements MqService {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public String sendMessage(String routingKey, String message) {
		this.rabbitTemplate.convertAndSend(routingKey, message);
		return message;
	}

	@Override
	public String sendMessage(String ex, String routingKey, String message) {
		this.rabbitTemplate.convertAndSend(ex, routingKey, message);
		return message;
	}

}
