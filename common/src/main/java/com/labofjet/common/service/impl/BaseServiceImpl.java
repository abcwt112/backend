package com.labofjet.common.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础service
 */
public class BaseServiceImpl {
	protected Logger log = LoggerFactory.getLogger(getClass());
}

