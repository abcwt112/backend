package com.labofjet.common.service;


import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.types.Expiration;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RedisService {
	String get(String key);

	Boolean set(String key, String value);

	Boolean set(String key, String value, Expiration expiration, RedisStringCommands.SetOption option);

	Long delete(String key);

	<K> Long delete(Collection<K> keys);

	Boolean exists(String key);

	Boolean expire(String key, Long expireTime);

	Boolean setnx(String key, String value);

	Void setex(String key, Long timeout, String value);

	Void setrange(String key, String value, Long offset);

	byte[] getrange(String key, Long start, Long end);

	Long incr(String key);

	Long decr(String key);

	Long incrby(String key, Long offset);

	Long decrby(String key, Long offset);

	Void mset(Map<byte[], byte[]> map);

	List<byte[]> mget(byte[]... bytes);

	Boolean msetnx(Map<byte[], byte[]> map);

	Long lpush(String key, String value);

	Long rpush(String key, String value);

	List<byte[]> lrange(String key, Long start, Long end);

	byte[] lindex(String key, Long offset);

	Long llen(String key);

	Long sadd(String key, byte[]... bytes);

	Set<byte[]> smembers(String key);

	Long srem(String key, byte[]... bytes);

	Set<byte[]> sinter(byte[]... bytes);

	Long sinterstore(String key, byte[]... bytes);

	Set<byte[]> sunion(byte[]... bytes);

	Long sunionstore(String key, byte[]... bytes);

	Boolean smove(String key1, String key2, String field);

	Long scard(String key);

	Boolean sismember(String key, String value);

	Boolean zadd(String key, Double offset, byte[] bytes2);

	Set<byte[]> zrange(String key, Long start, Long end);

	Long zrem(String key, byte[]... bytes);

	Double zincrby(String key, Double offset, String field);

	Set<byte[]> zrangebyscore(String key, Double start, Double end);

	Long zremrangebyrank(String key, Double start, Double end);

	Long zcount(String key, Double start, Double end);

	Boolean hset(String key, String field, String value);

	byte[] hget(String key, String field);

	Set<byte[]> hkeys(String key);

	Long hdel(String key, byte[]... fields);

	Long hlen(String key);

	Void hmset(String key, Map<byte[], byte[]> map);

	List<byte[]> hmget(String key, byte[]... fields);

	Double hincrby(String key, String field, Double val);

	Boolean hexists(String key, String field);

	List<byte[]> hvals(String key);

	Boolean hsetnx(String key, String field, String value);

	Map<byte[], byte[]> hgetall(String key);

	RedisTemplate<String, Object> getredisTemplate();

	RedisTemplate<String, Object> getRedisSlaveTemplate();

	RedisSerializer<String> getRedisSerializer();

}
