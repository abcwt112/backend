package com.labofjet.common.service;

public interface MqService {
	/**
	 * 向mq发送信息
	 *
	 * @param routingKey
	 * @param message
	 * @return
	 */
	String sendMessage(String routingKey, String message);

	String sendMessage(String ex, String routingKey, String message);
}
