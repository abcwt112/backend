package com.labofjet.log.collector.mq.handler;

import com.labofjet.common.dto.LogDto;
import com.labofjet.common.handler.impl.BaseMqHandlerImpl;
import com.labofjet.common.util.ConverterUtils;
import com.labofjet.common.util.IdUtils;
import com.labofjet.common.util.JSONUtils;
import com.labofjet.log.collector.mq.entity.LogCmp;
import com.labofjet.log.collector.mq.repository.LogLogRepository;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 收集日志,写入mongodb
 */
@Service
@RabbitListener(bindings = {
		@QueueBinding(value = @Queue(value = "logQueue", durable = "true"), exchange = @Exchange(value = "logExchange", durable = "true"))
})
public class LogRabbitMqLogHandler extends BaseMqHandlerImpl {


	@Resource
	private LogLogRepository logLogRepository;


	@Override
	public void handle(String s) {
		LogDto logDto = JSONUtils.stringToDto(s, LogDto.class);
		log.info("收到LogDto msg: " + logDto.toString());
		logDto.setId(IdUtils.uuid());
		logLogRepository.save(ConverterUtils.convertOne(logDto, new LogCmp()));
	}
}
