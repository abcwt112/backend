package com.labofjet.log.collector.mq.repository;

import com.labofjet.log.collector.mq.entity.LogCmp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogLogRepository extends MongoRepository<LogCmp, String> {


}
