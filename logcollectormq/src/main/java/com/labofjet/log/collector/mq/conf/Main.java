package com.labofjet.log.collector.mq.conf;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * 启动类
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.labofjet")
@EnableAutoConfiguration
//@EnableTransactionManagement(proxyTargetClass = true)
@EnableRabbit
@EnableMongoRepositories(basePackages = "com.labofjet")
@EnableDiscoveryClient
public class Main {

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

}