package com.labofjet.log.collector.mq.controller;

import com.labofjet.common.controller.BaseController;
import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.dto.LogDto;
import com.labofjet.common.dto.PageDto;
import com.labofjet.log.collector.mq.service.impl.LogLogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * common页面信息
 */
@RestController
public class LogLogController extends BaseController {


    @Autowired
    private LogLogServiceImpl logService;

    @RequestMapping("/log/list")
    public ContextDto list(@RequestBody ContextDto contextDto) {
        log.info("调用 LogLogController.list 参数为{} ", contextDto);
        contextDto.setSuccess(true);
        PageDto<LogDto> list = logService.list(contextDto);
        contextDto.setData(list);
        return contextDto;
    }

    @RequestMapping("/log/load")
    public ContextDto load(@RequestBody ContextDto contextDto) {
        log.info("调用 LogLogController.load 参数为{} ", contextDto);
        contextDto.setSuccess(true);
        LogDto dto = logService.load(contextDto);
        contextDto.setData(dto);
        return contextDto;
    }

}
