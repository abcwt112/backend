package com.labofjet.log.collector.mq.service.impl;

import com.labofjet.common.dto.ContextDto;
import com.labofjet.common.dto.LogDto;
import com.labofjet.common.dto.PageDto;
import com.labofjet.common.service.impl.BaseServiceImpl;
import com.labofjet.common.util.MongoQueryUtils;
import com.labofjet.log.collector.mq.entity.LogCmp;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LogLogServiceImpl extends BaseServiceImpl {

    @Resource
    private MongoTemplate mongoTemplate;


    public PageDto<LogDto> list(ContextDto contextDto) {

        PageDto<LogDto> page = MongoQueryUtils.getInstance(contextDto, mongoTemplate).and(MongoQueryUtils.getInstance(contextDto).end("dateEnd").getCriteria())
                .and("id").start("dateStart").in("levels", "level").and("uuid").and("ip").like("message").paging().find(LogCmp.class, LogDto.class);
        return page;
    }

    public LogDto load(ContextDto contextDto) {
        PageDto<LogDto> list = this.list(contextDto);
        if (list.getTotal() > 0) {
            return list.getList().get(0);
        }
        return null;
    }
}
